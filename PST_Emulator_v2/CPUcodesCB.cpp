#include "CPU.hpp"

void CPU::execute_CB_opcode(void)
{
	uint8_t opcode = read(reg_pc++);
	switch(opcode)
	{

	 case 0x00:
		 // RLC B
		 set_f((reg_b & 0x80) ? C_FLAG : 0);
		 reg_b = (reg_b << 1) | (reg_b >> 7);
		 set_f(get_f() | ZeroTable[reg_b]);
		 break;
	 case 0x01:
		 // RLC C
		 set_f((reg_c & 0x80) ? C_FLAG : 0);
		 reg_c = (reg_c << 1) | (reg_c >> 7);
		 set_f(get_f() | ZeroTable[reg_c]);
		 break;
	 case 0x02:
		 // RLC D
		 set_f((reg_d & 0x80) ? C_FLAG : 0);
		 reg_d = (reg_d << 1) | (reg_d >> 7);
		 set_f(get_f() | ZeroTable[reg_d]);
		 break;
	 case 0x03:
		 // RLC E
		 set_f((reg_e & 0x80) ? C_FLAG : 0);
		 reg_e = (reg_e << 1) | (reg_e >> 7);
		 set_f(get_f() | ZeroTable[reg_e]);
		 break;
	 case 0x04:
		 // RLC H
		 set_f((reg_h & 0x80) ? C_FLAG : 0);
		 reg_h = (reg_h << 1) | (reg_h >> 7);
		 set_f(get_f() | ZeroTable[reg_h]);
		 break;
	 case 0x05:
		 // RLC L
		 set_f((reg_l & 0x80) ? C_FLAG : 0);
		 reg_l = (reg_l << 1) | (reg_l >> 7);
		 set_f(get_f() | ZeroTable[reg_l]);
		 break;
	 case 0x06:
		 // RLC (HL)
		 tempValue = read(read_HL());
		 set_f((tempValue & 0x80) ? C_FLAG : 0);
		 tempValue = (tempValue << 1) | (tempValue >> 7);
		 set_f(get_f() | ZeroTable[tempValue]);
		 write(read_HL(), tempValue);
		 break;
	 case 0x07:
		 // RLC A
		 set_f((reg_a & 0x80) ? C_FLAG : 0);
		 reg_a = (reg_a << 1) | (reg_a >> 7);
		 set_f(get_f() | ZeroTable[reg_a]);
		 break;
	 case 0x08:
		 // RRC B
		 set_f(reg_b & 0x01 ? C_FLAG : 0);
		 reg_b = (reg_b >> 1) | (reg_b << 7);
		 set_f(get_f() | ZeroTable[reg_b]);
		 break;
	 case 0x09:
		 // RRC C
		 set_f(reg_c & 0x01 ? C_FLAG : 0);
		 reg_c = (reg_c >> 1) | (reg_c << 7);
		 set_f(get_f() | ZeroTable[reg_c]);
		 break;
	 case 0x0a:
		 // RRC D
		 set_f(reg_d & 0x01 ? C_FLAG : 0);
		 reg_d = (reg_d >> 1) | (reg_d << 7);
		 set_f(get_f() | ZeroTable[reg_d]);
		 break;
	 case 0x0b:
		 // RRC E
		 set_f(reg_e & 0x01 ? C_FLAG : 0);
		 reg_e = (reg_e >> 1) | (reg_e << 7);
		 set_f(get_f() | ZeroTable[reg_e]);
		 break;
	 case 0x0c:
		 // RRC H
		 set_f(reg_h & 0x01 ? C_FLAG : 0);
		 reg_h = (reg_h >> 1) | (reg_h << 7);
		 set_f(get_f() | ZeroTable[reg_h]);
		 break;
	 case 0x0d:
		 // RRC L
		 set_f(reg_l & 0x01 ? C_FLAG : 0);
		 reg_l = (reg_l >> 1) | (reg_l << 7);
		 set_f(get_f() | ZeroTable[reg_l]);
		 break;
	 case 0x0e:
		 // RRC (HL)
		 tempValue = read(read_HL());
		 set_f(tempValue & 0x01 ? C_FLAG : 0);
		 tempValue = (tempValue >> 1) | (tempValue << 7);
		 set_f(get_f() | ZeroTable[tempValue]);
		 write(read_HL(), tempValue);
		 break;
	 case 0x0f:
		 // RRC A
		 set_f(reg_a & 0x01 ? C_FLAG : 0);
		 reg_a = (reg_a >> 1) | (reg_a << 7);
		 set_f(get_f() | ZeroTable[reg_a]);
		 break;
	 case 0x10:
		 // RL B
		 if (reg_b & 0x80) {
			 reg_b = (reg_b << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_b] | C_FLAG);
		 }
		 else {
			 reg_b = (reg_b << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_b]);
		 }
		 break;
	 case 0x11:
		 // RL C
		 if (reg_c & 0x80) {
			 reg_c = (reg_c << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_c] | C_FLAG);
		 }
		 else {
			 reg_c = (reg_c << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_c]);
		 }
		 break;
	 case 0x12:
		 // RL D
		 if (reg_d & 0x80) {
			 reg_d = (reg_d << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_d] | C_FLAG);
		 }
		 else {
			 reg_d = (reg_d << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_d]);
		 }
		 break;
	 case 0x13:
		 // RL E
		 if (reg_e & 0x80) {
			 reg_e = (reg_e << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_e] | C_FLAG);
		 }
		 else {
			 reg_e = (reg_e << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_e]);
		 }
		 break;
	 case 0x14:
		 // RL H
		 if (reg_h & 0x80) {
			 reg_h = (reg_h << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_h] | C_FLAG);
		 }
		 else {
			 reg_h = (reg_h << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_h]);
		 }
		 break;
	 case 0x15:
		 // RL L
		 if (reg_l & 0x80) {
			 reg_l = (reg_l << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_l] | C_FLAG);
		 }
		 else {
			 reg_l = (reg_l << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_l]);
		 }
		 break;
	 case 0x16:
		 // RL (HL)
		 tempValue = read(read_HL());
		 if (tempValue & 0x80) {
			 tempValue = (tempValue << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[tempValue] | C_FLAG);
		 }
		 else {
			 tempValue = (tempValue << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[tempValue]);
		 }
		 write(read_HL(), tempValue);
		 break;
	 case 0x17:
		 // RL A
		 if (reg_a & 0x80) {
			 reg_a = (reg_a << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_a] | C_FLAG);
		 }
		 else {
			 reg_a = (reg_a << 1) | (get_f()&C_FLAG ? 1 : 0);
			 set_f(ZeroTable[reg_a]);
		 }
		 break;
	 case 0x18:
		 // RR B
		 if (reg_b & 0x01) {
			 reg_b = (reg_b >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_b] | C_FLAG);
		 }
		 else {
			 reg_b = (reg_b >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_b]);
		 }
		 break;
	 case 0x19:
		 // RR C
		 if (reg_c & 0x01) {
			 reg_c = (reg_c >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_c] | C_FLAG);
		 }
		 else {
			 reg_c = (reg_c >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_c]);
		 }
		 break;
	 case 0x1a:
		 // RR D
		 if (reg_d & 0x01) {
			 reg_d = (reg_d >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_d] | C_FLAG);
		 }
		 else {
			 reg_d = (reg_d >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_d]);
		 }
		 break;
	 case 0x1b:
		 // RR E
		 if (reg_e & 0x01) {
			 reg_e = (reg_e >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_e] | C_FLAG);
		 }
		 else {
			 reg_e = (reg_e >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_e]);
		 }
		 break;
	 case 0x1c:
		 // RR H
		 if (reg_h & 0x01) {
			 reg_h = (reg_h >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_h] | C_FLAG);
		 }
		 else {
			 reg_h = (reg_h >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_h]);
		 }
		 break;
	 case 0x1d:
		 // RR L
		 if (reg_l & 0x01) {
			 reg_l = (reg_l >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_l] | C_FLAG);
		 }
		 else {
			 reg_l = (reg_l >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_l]);
		 }
		 break;
	 case 0x1e:
		 // RR (HL)
		 tempValue = read(read_HL());
		 if (tempValue & 0x01) {
			 tempValue = (tempValue >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[tempValue] | C_FLAG);
		 }
		 else {
			 tempValue = (tempValue >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[tempValue]);
		 }
		 write(read_HL(), tempValue);
		 break;
	 case 0x1f:
		 // RR A
		 if (reg_a & 0x01) {
			 reg_a = (reg_a >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_a] | C_FLAG);
		 }
		 else {
			 reg_a = (reg_a >> 1) | (get_f() & C_FLAG ? 0x80 : 0);
			 set_f(ZeroTable[reg_a]);
		 }
		 break;
	 case 0x20:
		 // SLA B
		 set_f(reg_b & 0x80 ? C_FLAG : 0);
		 reg_b <<= 1;
		 set_f(get_f() | ZeroTable[reg_b]);
		 break;
	 case 0x21:
		 // SLA C
		 set_f(reg_c & 0x80 ? C_FLAG : 0);
		 reg_c <<= 1;
		 set_f(get_f() | ZeroTable[reg_c]);
		 break;
	 case 0x22:
		 // SLA D
		 set_f(reg_d & 0x80 ? C_FLAG : 0);
		 reg_d <<= 1;
		 set_f(get_f() | ZeroTable[reg_d]);
		 break;
	 case 0x23:
		 // SLA E
		 set_f(reg_e & 0x80 ? C_FLAG : 0);
		 reg_e <<= 1;
		 set_f(get_f() | ZeroTable[reg_e]);
		 break;
	 case 0x24:
		 // SLA H
		 set_f(reg_h & 0x80 ? C_FLAG : 0);
		 reg_h <<= 1;
		 set_f(get_f() | ZeroTable[reg_h]);
		 break;
	 case 0x25:
		 // SLA L
		 set_f(reg_l & 0x80 ? C_FLAG : 0);
		 reg_l <<= 1;
		 set_f(get_f() | ZeroTable[reg_l]);
		 break;
	 case 0x26:
		 // SLA (HL)
		 tempValue = read(read_HL());
		 set_f(tempValue & 0x80 ? C_FLAG : 0);
		 tempValue <<= 1;
		 set_f(get_f() | ZeroTable[tempValue]);
		 write(read_HL(), tempValue);
		 break;
	 case 0x27:
		 // SLA A
		 set_f(reg_a & 0x80 ? C_FLAG : 0);
		 reg_a <<= 1;
		 set_f(get_f() | ZeroTable[reg_a]);
		 break;
	 case 0x28:
		 // SRA B
		 set_f(reg_b & 0x01 ? C_FLAG : 0);
		 reg_b = (reg_b >> 1) | (reg_b & 0x80);
		 set_f(get_f() | ZeroTable[reg_b]);
		 break;
	 case 0x29:
		 // SRA C
		 set_f(reg_c & 0x01 ? C_FLAG : 0);
		 reg_c = (reg_c >> 1) | (reg_c & 0x80);
		 set_f(get_f() | ZeroTable[reg_c]);
		 break;
	 case 0x2a:
		 // SRA D
		 set_f(reg_d & 0x01 ? C_FLAG : 0);
		 reg_d = (reg_d >> 1) | (reg_d & 0x80);
		 set_f(get_f() | ZeroTable[reg_d]);
		 break;
	 case 0x2b:
		 // SRA E
		 set_f(reg_e & 0x01 ? C_FLAG : 0);
		 reg_e = (reg_e >> 1) | (reg_e & 0x80);
		 set_f(get_f() | ZeroTable[reg_e]);
		 break;
	 case 0x2c:
		 // SRA H
		 set_f(reg_h & 0x01 ? C_FLAG : 0);
		 reg_h = (reg_h >> 1) | (reg_h & 0x80);
		 set_f(get_f() | ZeroTable[reg_h]);
		 break;
	 case 0x2d:
		 // SRA L
		 set_f(reg_l & 0x01 ? C_FLAG : 0);
		 reg_l = (reg_l >> 1) | (reg_l & 0x80);
		 set_f(get_f() | ZeroTable[reg_l]);
		 break;
	 case 0x2e:
		 // SRA (HL)
		 tempValue = read(read_HL());
		 set_f(tempValue & 0x01 ? C_FLAG : 0);
		 tempValue = (tempValue >> 1) | (tempValue & 0x80);
		 set_f(get_f() | ZeroTable[tempValue]);
		 write(read_HL(), tempValue);
		 break;
	 case 0x2f:
		 // SRA A
		 set_f(reg_a & 0x01 ? C_FLAG : 0);
		 reg_a = (reg_a >> 1) | (reg_a & 0x80);
		 set_f(get_f() | ZeroTable[reg_a]);
		 break;
	 case 0x30:
		 // SWAP B
		 reg_b = (reg_b & 0xf0) >> 4 | (reg_b & 0x0f) << 4;
		 set_f(ZeroTable[reg_b]);
		 break;
	 case 0x31:
		 // SWAP C
		 reg_c = (reg_c & 0xf0) >> 4 | (reg_c & 0x0f) << 4;
		 set_f(ZeroTable[reg_c]);
		 break;
	 case 0x32:
		 // SWAP D
		 reg_d = (reg_d & 0xf0) >> 4 | (reg_d & 0x0f) << 4;
		 set_f(ZeroTable[reg_d]);
		 break;
	 case 0x33:
		 // SWAP E
		 reg_e = (reg_e & 0xf0) >> 4 | (reg_e & 0x0f) << 4;
		 set_f(ZeroTable[reg_e]);
		 break;
	 case 0x34:
		 // SWAP H
		 reg_h = (reg_h & 0xf0) >> 4 | (reg_h & 0x0f) << 4;
		 set_f(ZeroTable[reg_h]);
		 break;
	 case 0x35:
		 // SWAP L
		 reg_l = (reg_l & 0xf0) >> 4 | (reg_l & 0x0f) << 4;
		 set_f(ZeroTable[reg_l]);
		 break;
	 case 0x36:
		 // SWAP (HL)
		 tempValue = read(read_HL());
		 tempValue = (tempValue & 0xf0) >> 4 | (tempValue & 0x0f) << 4;
		 set_f(ZeroTable[tempValue]);
		 write(read_HL(), tempValue);
		 break;
	 case 0x37:
		 // SWAP A
		 reg_a = (reg_a & 0xf0) >> 4 | (reg_a & 0x0f) << 4;
		 set_f(ZeroTable[reg_a]);
		 break;
	 case 0x38:
		 // SRL B
		 set_f((reg_b & 0x01) ? C_FLAG : 0);
		 reg_b >>= 1;
		 set_f(get_f() | ZeroTable[reg_b]);
		 break;
	 case 0x39:
		 // SRL C
		 set_f((reg_c & 0x01) ? C_FLAG : 0);
		 reg_c >>= 1;
		 set_f(get_f() | ZeroTable[reg_c]);
		 break;
	 case 0x3a:
		 // SRL D
		 set_f((reg_d & 0x01) ? C_FLAG : 0);
		 reg_d >>= 1;
		 set_f(get_f() | ZeroTable[reg_d]);
		 break;
	 case 0x3b:
		 // SRL E
		 set_f((reg_e & 0x01) ? C_FLAG : 0);
		 reg_e >>= 1;
		 set_f(get_f() | ZeroTable[reg_e]);
		 break;
	 case 0x3c:
		 // SRL H
		 set_f((reg_h & 0x01) ? C_FLAG : 0);
		 reg_h >>= 1;
		 set_f(get_f() | ZeroTable[reg_h]);
		 break;
	 case 0x3d:
		 // SRL L
		 set_f((reg_l & 0x01) ? C_FLAG : 0);
		 reg_l >>= 1;
		 set_f(get_f() | ZeroTable[reg_l]);
		 break;
	 case 0x3e:
		 // SRL (HL)
		 tempValue = read(read_HL());
		 set_f((tempValue & 0x01) ? C_FLAG : 0);
		 tempValue >>= 1;
		 set_f(get_f() | ZeroTable[tempValue]);
		 write(read_HL(), tempValue);
		 break;
	 case 0x3f:
		 // SRL A
		 set_f((reg_a & 0x01) ? C_FLAG : 0);
		 reg_a >>= 1;
		 set_f(get_f() | ZeroTable[reg_a]);
		 break;
	 case 0x40:
		 // BIT 0,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x41:
		 // BIT 0,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x42:
		 // BIT 0,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x43:
		 // BIT 0,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x44:
		 // BIT 0,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x45:
		 // BIT 0,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x46:
		 // BIT 0,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x47:
		 // BIT 0,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 0) ? 0 : Z_FLAG));
		 break;
	 case 0x48:
		 // BIT 1,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x49:
		 // BIT 1,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x4a:
		 // BIT 1,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x4b:
		 // BIT 1,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x4c:
		 // BIT 1,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x4d:
		 // BIT 1,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x4e:
		 // BIT 1,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x4f:
		 // BIT 1,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 1) ? 0 : Z_FLAG));
		 break;
	 case 0x50:
		 // BIT 2,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x51:
		 // BIT 2,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x52:
		 // BIT 2,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x53:
		 // BIT 2,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x54:
		 // BIT 2,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x55:
		 // BIT 2,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x56:
		 // BIT 2,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x57:
		 // BIT 2,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 2) ? 0 : Z_FLAG));
		 break;
	 case 0x58:
		 // BIT 3,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x59:
		 // BIT 3,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x5a:
		 // BIT 3,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x5b:
		 // BIT 3,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x5c:
		 // BIT 3,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x5d:
		 // BIT 3,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x5e:
		 // BIT 3,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x5f:
		 // BIT 3,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 3) ? 0 : Z_FLAG));
		 break;
	 case 0x60:
		 // BIT 4,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x61:
		 // BIT 4,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x62:
		 // BIT 4,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x63:
		 // BIT 4,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x64:
		 // BIT 4,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x65:
		 // BIT 4,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x66:
		 // BIT 4,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x67:
		 // BIT 4,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 4) ? 0 : Z_FLAG));
		 break;
	 case 0x68:
		 // BIT 5,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x69:
		 // BIT 5,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x6a:
		 // BIT 5,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x6b:
		 // BIT 5,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x6c:
		 // BIT 5,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x6d:
		 // BIT 5,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x6e:
		 // BIT 5,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x6f:
		 // BIT 5,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 5) ? 0 : Z_FLAG));
		 break;
	 case 0x70:
		 // BIT 6,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x71:
		 // BIT 6,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x72:
		 // BIT 6,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x73:
		 // BIT 6,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x74:
		 // BIT 6,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x75:
		 // BIT 6,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x76:
		 // BIT 6,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x77:
		 // BIT 6,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 6) ? 0 : Z_FLAG));
		 break;
	 case 0x78:
		 // BIT 7,B
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_b&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x79:
		 // BIT 7,C
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_c&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x7a:
		 // BIT 7,D
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_d&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x7b:
		 // BIT 7,E
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_e&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x7c:
		 // BIT 7,H
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_h&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x7d:
		 // BIT 7,L
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_l&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x7e:
		 // BIT 7,(HL)
		 tempValue = read(read_HL());
		 set_f((get_f()&C_FLAG) | H_FLAG | (tempValue&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x7f:
		 // BIT 7,A
		 set_f((get_f()&C_FLAG) | H_FLAG | (reg_a&(1 << 7) ? 0 : Z_FLAG));
		 break;
	 case 0x80:
		 // RES 0,B
		 reg_b &= ~(1 << 0);
		 break;
	 case 0x81:
		 // RES 0,C
		 reg_c &= ~(1 << 0);
		 break;
	 case 0x82:
		 // RES 0,D
		 reg_d &= ~(1 << 0);
		 break;
	 case 0x83:
		 // RES 0,E
		 reg_e &= ~(1 << 0);
		 break;
	 case 0x84:
		 // RES 0,H
		 reg_h &= ~(1 << 0);
		 break;
	 case 0x85:
		 // RES 0,L
		 reg_l &= ~(1 << 0);
		 break;
	 case 0x86:
		 // RES 0,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 0);
		 write(read_HL(), tempValue);
		 break;
	 case 0x87:
		 // RES 0,A
		 reg_a &= ~(1 << 0);
		 break;
	 case 0x88:
		 // RES 1,B
		 reg_b &= ~(1 << 1);
		 break;
	 case 0x89:
		 // RES 1,C
		 reg_c &= ~(1 << 1);
		 break;
	 case 0x8a:
		 // RES 1,D
		 reg_d &= ~(1 << 1);
		 break;
	 case 0x8b:
		 // RES 1,E
		 reg_e &= ~(1 << 1);
		 break;
	 case 0x8c:
		 // RES 1,H
		 reg_h &= ~(1 << 1);
		 break;
	 case 0x8d:
		 // RES 1,L
		 reg_l &= ~(1 << 1);
		 break;
	 case 0x8e:
		 // RES 1,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 1);
		 write(read_HL(), tempValue);
		 break;
	 case 0x8f:
		 // RES 1,A
		 reg_a &= ~(1 << 1);
		 break;
	 case 0x90:
		 // RES 2,B
		 reg_b &= ~(1 << 2);
		 break;
	 case 0x91:
		 // RES 2,C
		 reg_c &= ~(1 << 2);
		 break;
	 case 0x92:
		 // RES 2,D
		 reg_d &= ~(1 << 2);
		 break;
	 case 0x93:
		 // RES 2,E
		 reg_e &= ~(1 << 2);
		 break;
	 case 0x94:
		 // RES 2,H
		 reg_h &= ~(1 << 2);
		 break;
	 case 0x95:
		 // RES 2,L
		 reg_l &= ~(1 << 2);
		 break;
	 case 0x96:
		 // RES 2,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 2);
		 write(read_HL(), tempValue);
		 break;
	 case 0x97:
		 // RES 2,A
		 reg_a &= ~(1 << 2);
		 break;
	 case 0x98:
		 // RES 3,B
		 reg_b &= ~(1 << 3);
		 break;
	 case 0x99:
		 // RES 3,C
		 reg_c &= ~(1 << 3);
		 break;
	 case 0x9a:
		 // RES 3,D
		 reg_d &= ~(1 << 3);
		 break;
	 case 0x9b:
		 // RES 3,E
		 reg_e &= ~(1 << 3);
		 break;
	 case 0x9c:
		 // RES 3,H
		 reg_h &= ~(1 << 3);
		 break;
	 case 0x9d:
		 // RES 3,L
		 reg_l &= ~(1 << 3);
		 break;
	 case 0x9e:
		 // RES 3,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 3);
		 write(read_HL(), tempValue);
		 break;
	 case 0x9f:
		 // RES 3,A
		 reg_a &= ~(1 << 3);
		 break;
	 case 0xa0:
		 // RES 4,B
		 reg_b &= ~(1 << 4);
		 break;
	 case 0xa1:
		 // RES 4,C
		 reg_c &= ~(1 << 4);
		 break;
	 case 0xa2:
		 // RES 4,D
		 reg_d &= ~(1 << 4);
		 break;
	 case 0xa3:
		 // RES 4,E
		 reg_e &= ~(1 << 4);
		 break;
	 case 0xa4:
		 // RES 4,H
		 reg_h &= ~(1 << 4);
		 break;
	 case 0xa5:
		 // RES 4,L
		 reg_l &= ~(1 << 4);
		 break;
	 case 0xa6:
		 // RES 4,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 4);
		 write(read_HL(), tempValue);
		 break;
	 case 0xa7:
		 // RES 4,A
		 reg_a &= ~(1 << 4);
		 break;
	 case 0xa8:
		 // RES 5,B
		 reg_b &= ~(1 << 5);
		 break;
	 case 0xa9:
		 // RES 5,C
		 reg_c &= ~(1 << 5);
		 break;
	 case 0xaa:
		 // RES 5,D
		 reg_d &= ~(1 << 5);
		 break;
	 case 0xab:
		 // RES 5,E
		 reg_e &= ~(1 << 5);
		 break;
	 case 0xac:
		 // RES 5,H
		 reg_h &= ~(1 << 5);
		 break;
	 case 0xad:
		 // RES 5,L
		 reg_l &= ~(1 << 5);
		 break;
	 case 0xae:
		 // RES 5,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 5);
		 write(read_HL(), tempValue);
		 break;
	 case 0xaf:
		 // RES 5,A
		 reg_a &= ~(1 << 5);
		 break;
	 case 0xb0:
		 // RES 6,B
		 reg_b &= ~(1 << 6);
		 break;
	 case 0xb1:
		 // RES 6,C
		 reg_c &= ~(1 << 6);
		 break;
	 case 0xb2:
		 // RES 6,D
		 reg_d &= ~(1 << 6);
		 break;
	 case 0xb3:
		 // RES 6,E
		 reg_e &= ~(1 << 6);
		 break;
	 case 0xb4:
		 // RES 6,H
		 reg_h &= ~(1 << 6);
		 break;
	 case 0xb5:
		 // RES 6,L
		 reg_l &= ~(1 << 6);
		 break;
	 case 0xb6:
		 // RES 6,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 6);
		 write(read_HL(), tempValue);
		 break;
	 case 0xb7:
		 // RES 6,A
		 reg_a &= ~(1 << 6);
		 break;
	 case 0xb8:
		 // RES 7,B
		 reg_b &= ~(1 << 7);
		 break;
	 case 0xb9:
		 // RES 7,C
		 reg_c &= ~(1 << 7);
		 break;
	 case 0xba:
		 // RES 7,D
		 reg_d &= ~(1 << 7);
		 break;
	 case 0xbb:
		 // RES 7,E
		 reg_e &= ~(1 << 7);
		 break;
	 case 0xbc:
		 // RES 7,H
		 reg_h &= ~(1 << 7);
		 break;
	 case 0xbd:
		 // RES 7,L
		 reg_l &= ~(1 << 7);
		 break;
	 case 0xbe:
		 // RES 7,(HL)
		 tempValue = read(read_HL());
		 tempValue &= ~(1 << 7);
		 write(read_HL(), tempValue);
		 break;
	 case 0xbf:
		 // RES 7,A
		 reg_a &= ~(1 << 7);
		 break;
	 case 0xc0:
		 // SET 0,B
		 reg_b |= 1 << 0;
		 break;
	 case 0xc1:
		 // SET 0,C
		 reg_c |= 1 << 0;
		 break;
	 case 0xc2:
		 // SET 0,D
		 reg_d |= 1 << 0;
		 break;
	 case 0xc3:
		 // SET 0,E
		 reg_e |= 1 << 0;
		 break;
	 case 0xc4:
		 // SET 0,H
		 reg_h |= 1 << 0;
		 break;
	 case 0xc5:
		 // SET 0,L
		 reg_l |= 1 << 0;
		 break;
	 case 0xc6:
		 // SET 0,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 0;
		 write(read_HL(), tempValue);
		 break;
	 case 0xc7:
		 // SET 0,A
		 reg_a |= 1 << 0;
		 break;
	 case 0xc8:
		 // SET 1,B
		 reg_b |= 1 << 1;
		 break;
	 case 0xc9:
		 // SET 1,C
		 reg_c |= 1 << 1;
		 break;
	 case 0xca:
		 // SET 1,D
		 reg_d |= 1 << 1;
		 break;
	 case 0xcb:
		 // SET 1,E
		 reg_e |= 1 << 1;
		 break;
	 case 0xcc:
		 // SET 1,H
		 reg_h |= 1 << 1;
		 break;
	 case 0xcd:
		 // SET 1,L
		 reg_l |= 1 << 1;
		 break;
	 case 0xce:
		 // SET 1,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 1;
		 write(read_HL(), tempValue);
		 break;
	 case 0xcf:
		 // SET 1,A
		 reg_a |= 1 << 1;
		 break;
	 case 0xd0:
		 // SET 2,B
		 reg_b |= 1 << 2;
		 break;
	 case 0xd1:
		 // SET 2,C
		 reg_c |= 1 << 2;
		 break;
	 case 0xd2:
		 // SET 2,D
		 reg_d |= 1 << 2;
		 break;
	 case 0xd3:
		 // SET 2,E
		 reg_e |= 1 << 2;
		 break;
	 case 0xd4:
		 // SET 2,H
		 reg_h |= 1 << 2;
		 break;
	 case 0xd5:
		 // SET 2,L
		 reg_l |= 1 << 2;
		 break;
	 case 0xd6:
		 // SET 2,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 2;
		 write(read_HL(), tempValue);
		 break;
	 case 0xd7:
		 // SET 2,A
		 reg_a |= 1 << 2;
		 break;
	 case 0xd8:
		 // SET 3,B
		 reg_b |= 1 << 3;
		 break;
	 case 0xd9:
		 // SET 3,C
		 reg_c |= 1 << 3;
		 break;
	 case 0xda:
		 // SET 3,D
		 reg_d |= 1 << 3;
		 break;
	 case 0xdb:
		 // SET 3,E
		 reg_e |= 1 << 3;
		 break;
	 case 0xdc:
		 // SET 3,H
		 reg_h |= 1 << 3;
		 break;
	 case 0xdd:
		 // SET 3,L
		 reg_l |= 1 << 3;
		 break;
	 case 0xde:
		 // SET 3,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 3;
		 write(read_HL(), tempValue);
		 break;
	 case 0xdf:
		 // SET 3,A
		 reg_a |= 1 << 3;
		 break;
	 case 0xe0:
		 // SET 4,B
		 reg_b |= 1 << 4;
		 break;
	 case 0xe1:
		 // SET 4,C
		 reg_c |= 1 << 4;
		 break;
	 case 0xe2:
		 // SET 4,D
		 reg_d |= 1 << 4;
		 break;
	 case 0xe3:
		 // SET 4,E
		 reg_e |= 1 << 4;
		 break;
	 case 0xe4:
		 // SET 4,H
		 reg_h |= 1 << 4;
		 break;
	 case 0xe5:
		 // SET 4,L
		 reg_l |= 1 << 4;
		 break;
	 case 0xe6:
		 // SET 4,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 4;
		 write(read_HL(), tempValue);
		 break;
	 case 0xe7:
		 // SET 4,A
		 reg_a |= 1 << 4;
		 break;
	 case 0xe8:
		 // SET 5,B
		 reg_b |= 1 << 5;
		 break;
	 case 0xe9:
		 // SET 5,C
		 reg_c |= 1 << 5;
		 break;
	 case 0xea:
		 // SET 5,D
		 reg_d |= 1 << 5;
		 break;
	 case 0xeb:
		 // SET 5,E
		 reg_e |= 1 << 5;
		 break;
	 case 0xec:
		 // SET 5,H
		 reg_h |= 1 << 5;
		 break;
	 case 0xed:
		 // SET 5,L
		 reg_l |= 1 << 5;
		 break;
	 case 0xee:
		 // SET 5,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 5;
		 write(read_HL(), tempValue);
		 break;
	 case 0xef:
		 // SET 5,A
		 reg_a |= 1 << 5;
		 break;
	 case 0xf0:
		 // SET 6,B
		 reg_b |= 1 << 6;
		 break;
	 case 0xf1:
		 // SET 6,C
		 reg_c |= 1 << 6;
		 break;
	 case 0xf2:
		 // SET 6,D
		 reg_d |= 1 << 6;
		 break;
	 case 0xf3:
		 // SET 6,E
		 reg_e |= 1 << 6;
		 break;
	 case 0xf4:
		 // SET 6,H
		 reg_h |= 1 << 6;
		 break;
	 case 0xf5:
		 // SET 6,L
		 reg_l |= 1 << 6;
		 break;
	 case 0xf6:
		 // SET 6,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 6;
		 write(read_HL(), tempValue);
		 break;
	 case 0xf7:
		 // SET 6,A
		 reg_a |= 1 << 6;
		 break;
	 case 0xf8:
		 // SET 7,B
		 reg_b |= 1 << 7;
		 break;
	 case 0xf9:
		 // SET 7,C
		 reg_c |= 1 << 7;
		 break;
	 case 0xfa:
		 // SET 7,D
		 reg_d |= 1 << 7;
		 break;
	 case 0xfb:
		 // SET 7,E
		 reg_e |= 1 << 7;
		 break;
	 case 0xfc:
		 // SET 7,H
		 reg_h |= 1 << 7;
		 break;
	 case 0xfd:
		 // SET 7,L
		 reg_l |= 1 << 7;
		 break;
	 case 0xfe:
		 // SET 7,(HL)
		 tempValue = read(read_HL());
		 tempValue |= 1 << 7;
		 write(read_HL(), tempValue);
		 break;
	 case 0xff:
		 // SET 7,A
		 reg_a |= 1 << 7;
		 break;
	 default:
		 printf("Unknown opcode %02x at %04x", opcode, read(reg_pc - 1));
		 /*if (gbSystemMessage == false)
		 {
			 systemMessage(0, N_("Unknown opcode %02x at %04x"),
				 gbReadOpcode(PC.W - 1), PC.W - 1);
			 gbSystemMessage = true;
		 }*/
		 break;
	}
	if ((opcode & 0x0f) == 0x6 || (opcode & 0x0f) == 0xe)
		t = 16;
	else
		t = 8;
}