#include "CPU.hpp"


void CPU::execute_opcode(void)
{
	uint8_t opcode = read(reg_pc++);

	switch (opcode)
	{

	case 0x00:
		// NOP
		t = 4;
		break;
	case 0x01:
		// LD BC, NNNN
		reg_c = read(reg_pc++);
		reg_b = read(reg_pc++);
		t = 12;
		break;
	case 0x02:
		// LD (BC),A
		write(read_BC(), reg_a);
		t = 8;
		break;
	case 0x03:
		// INC BC
		write_BC(read_BC() + 1);
		t = 8;
		break;
	case 0x04:
		// INC B
		++reg_b;
		set_f((get_f() & C_FLAG) | ZeroTable[reg_b] | (reg_b & 0x0F ? 0 : H_FLAG));
		t = 4;
		break;
	case 0x05:
		// DEC B
		--reg_b;
		set_f( N_FLAG | (get_f() & C_FLAG) | ZeroTable[reg_b] |
			((reg_b & 0x0F) == 0x0F ? H_FLAG : 0));
		t = 4;
		break;
	case 0x06:
		// LD B, NN
		reg_b = read(reg_pc++);
		t = 8;
		break;
	case 0x07:
		// RLCA
		tempValue = reg_a & 0x80 ? C_FLAG : 0;
		reg_a = (reg_a << 1) | (reg_a >> 7);
		set_f(tempValue);
		t = 4;
		break;
	case 0x08:
		// LD (NNNN), SP
		tempRegister.W = read(reg_pc++);
		tempRegister.W += read(reg_pc++) << 8;
		write(tempRegister.W, (uint8_t)reg_sp);
		++tempRegister.W;
		write(tempRegister.W, (uint8_t)(reg_sp >> 8));
		t = 20;
		break;
	case 0x09:
		// ADD HL,BC
		tempRegister.W = (read_HL() + read_BC()) & 0xFFFF;
		set_f((get_f() & Z_FLAG) | ((read_HL()^read_BC()^tempRegister.W) & 0x1000 ? H_FLAG : 0) |
			(((long)read_HL() + (long)read_BC()) & 0x10000 ? C_FLAG : 0));
		write_HL(tempRegister.W);
		t = 8;
		break;
	case 0x0a:
		// LD A,(BC)
		reg_a = read(read_BC());
		t = 8;
		break;
	case 0x0b:
		// DEC BC
		write_BC(read_BC() - 1);
		t = 8;
		break;
	case 0x0c:
		// INC C
		reg_c++;
		set_f((get_f() & C_FLAG) | ZeroTable[reg_c] | (reg_c & 0x0F ? 0 : H_FLAG));
		t = 4;
		break;
	case 0x0d:
		// DEC C
		reg_c--;
		set_f(N_FLAG | (get_f() & C_FLAG) | ZeroTable[reg_c] |
			((reg_c & 0x0F) == 0x0F ? H_FLAG : 0));
		t = 4;
		break;
	case 0x0e:
		// LD C, NN
		reg_c = read(reg_pc++);
		t = 8;
		break;
	case 0x0f:
		// RRCA
		tempValue = reg_a & 0x01;
		reg_a = (reg_a >> 1) | (tempValue ? 0x80 : 0);
		set_f(tempValue << 4);
		t = 4;
		break;
	case 0x10:
		// STOP
		/*opcode = read(reg_pc++);
		if (gbCgbMode) {
			if (gbMemory[0xff4d] & 1) {

				gbSpeedSwitch();
				//clockTicks += 228*144-(gbSpeed ? 62 : 63);

				if (gbSpeed == 0)
					gbMemory[0xff4d] = 0x00;
				else
					gbMemory[0xff4d] = 0x80;
			}
		}*/
		break;
	case 0x11:
		// LD DE, NNNN
		reg_e = read(reg_pc++);
		reg_d = read(reg_pc++);
		t = 12;
		break;
	case 0x12:
		// LD (DE),A
		write(read_DE(), reg_a);
		t = 8;
		break;
	case 0x13:
		// INC DE
		write_DE(read_DE() + 1);
		t = 8;
		break;
	case 0x14:
		// INC D
		reg_d++;
		set_f((get_f() & C_FLAG) | ZeroTable[reg_d] | (reg_d & 0x0F ? 0 : H_FLAG));
		t = 4;
		break;
	case 0x15:
		// DEC D
		reg_d--;
		set_f(N_FLAG | (get_f() & C_FLAG) | ZeroTable[reg_d] |
			((reg_d & 0x0F) == 0x0F ? H_FLAG : 0));
		t = 4;
		break;
	case 0x16:
		//  LD D,NN
		reg_d = read(reg_pc++);
		t = 8;
		break;
	case 0x17:
		// RLA
		tempValue = reg_a & 0x80 ? C_FLAG : 0;
		reg_a = (reg_a << 1) | ((get_f()&C_FLAG) >> 4);
		set_f(tempValue);
		t = 4;
		break;
	case 0x18:
		// JR NN
		reg_pc += (int8_t)read(reg_pc) + 1;
		t = 12;
		break;
	case 0x19:
		// ADD HL,DE
		tempRegister.W = (read_HL() + read_DE()) & 0xFFFF;
		set_f((get_f() & Z_FLAG) | ((read_HL()^read_DE()^tempRegister.W) & 0x1000 ? H_FLAG : 0) |
			(((long)read_HL() + (long)read_DE()) & 0x10000 ? C_FLAG : 0));
		write_HL(tempRegister.W);
		t = 8;
		break;
	case 0x1a:
		// LD A,(DE)
		reg_a = read(read_DE());
		t = 8;
		break;
	case 0x1b:
		// DEC DE
		write_DE(read_DE() - 1);
		t = 8;
		break;
	case 0x1c:
		// INC E
		reg_e++;
		set_f((get_f() & C_FLAG) | ZeroTable[reg_e] | (reg_e & 0x0F ? 0 : H_FLAG));
		t = 4;
		break;
	case 0x1d:
		// DEC E
		reg_e--;
		set_f(N_FLAG | (get_f() & C_FLAG) | ZeroTable[reg_e] |
			((reg_e & 0x0F) == 0x0F ? H_FLAG : 0));
		t = 4;
		break;
	case 0x1e:
		// LD E,NN
		reg_e = read(reg_pc++);
		t = 8;
		break;
	case 0x1f:
		// RRA
		tempValue = reg_a & 0x01;
		reg_a = (reg_a >> 1) | (get_f()&C_FLAG ? 0x80 : 0);
		set_f(tempValue << 4);
		t = 4;
		break;
	case 0x20:
		// JR NZ,NN
		if (get_f()&Z_FLAG)
		{
			reg_pc++;
			t = 8;
		}
		else {
			reg_pc += (int8_t)(read(reg_pc++));
			t = 12;
		}
		break;
	case 0x21:
		// LD HL,NNNN
		reg_l = read(reg_pc++);
		reg_h = read(reg_pc++);
		t = 12;
		break;
	case 0x22:
		// LDI (HL),A
		write(read_HL(), reg_a);
		write_HL(read_HL() + 1);
		t = 8;
		break;
	case 0x23:
		// INC HL
		write_HL(read_HL() + 1);
		t = 8;
		break;
	case 0x24:
		// INC H
		reg_h++;
		set_f((get_f() & C_FLAG) | ZeroTable[reg_h] | (reg_h & 0x0F ? 0 : H_FLAG));
		t = 4;
		break;
	case 0x25:
		// DEC H
		reg_h--;
		set_f(N_FLAG | (get_f() & C_FLAG) | ZeroTable[reg_h] |
			((reg_h & 0x0F) == 0x0F ? H_FLAG : 0));
		t = 4;
		break;
	case 0x26:
		// LD H,NN
		reg_h = read(reg_pc++);
		t = 8;
		break;
	case 0x27:
		// DAA
		a = reg_a;
		if (flag_negative == 0)
		{
			if (flag_half_carry == 1 || ((a & 0xF) > 9))
				a += 0x06;
			if (flag_carry == 1 || (a > 0x9F))
				a += 0x60;
		}
		else
		{
			if (flag_half_carry == 1)
				a = (a - 6) & 0xFF;
			if (flag_carry == 1)
				a -= 0x60;
		}
		flag_half_carry = 0;
		flag_zero = 0;
		if ((a & 0x100) == 0x100)
			flag_carry = 1;
		a &= 0xFF;
		if (a == 0)
			flag_zero = 1;
		reg_a = a;
		t = 4;
		break;
	case 0x28:
		// JR Z,NN
		if (get_f()&Z_FLAG) {
			reg_pc += (int8_t)read(reg_pc) + 1;
			t = 12;
		}
		else
		{
			reg_pc++;
			t = 8;
		}
		break;
	case 0x29:
		// ADD HL,HL
		tempRegister.W = (read_HL() + read_HL()) & 0xFFFF; set_f((get_f() & Z_FLAG) |
			((read_HL()^read_HL()^tempRegister.W) & 0x1000 ? H_FLAG : 0) |
			(((long)read_HL() + (long)read_HL()) & 0x10000 ? C_FLAG : 0));
		write_HL(tempRegister.W);
		t = 8;
		break;
	case 0x2a:
		// LDI A,(HL)
		reg_a = read(read_HL());
		write_HL(read_HL() + 1);
		t = 8;
		break;
	case 0x2b:
		// DEC HL
		write_HL(read_HL() - 1);
		t = 8;
		break;
	case 0x2c:
		// INC L
		reg_l++;
		set_f((get_f() & C_FLAG) | ZeroTable[reg_l] | (reg_l & 0x0F ? 0 : H_FLAG));
		t = 4;
		break;
	case 0x2d:
		// DEC L
		reg_l--;
		set_f(N_FLAG | (get_f() & C_FLAG) | ZeroTable[reg_l] |
			((reg_l & 0x0F) == 0x0F ? H_FLAG : 0));
		t = 4;
		break;
	case 0x2e:
		// LD L,NN
		reg_l = read(reg_pc++);
		t = 8;
		break;
	case 0x2f:
		// CPL
		reg_a ^= 255;
		set_f(get_f() | (N_FLAG | H_FLAG));
		t = 4;
		break;
	case 0x30:
		// JR NC,NN
		if (get_f()&C_FLAG)
		{
			reg_pc++;
			t = 8;
		}
		else {
			reg_pc += (int8_t)read(reg_pc) + 1;
			t = 12;
		}
		break;
	case 0x31:
		// LD SP,NNNN
		tempRegister.W = read(reg_pc++);
		tempRegister.W += read(reg_pc++) << 8;
		reg_sp = tempRegister.W;
		t = 12;
		break;
	case 0x32:
		// LDD (HL),A
		write(read_HL(), reg_a);
		write_HL(read_HL() - 1);
		t = 8;
		break;
	case 0x33:
		// INC SP
		reg_sp++;
		t = 8;
		break;
	case 0x34:
		// INC (HL)
		tempValue = read(read_HL()) + 1;
		set_f((get_f() & C_FLAG) | ZeroTable[tempValue] | (tempValue & 0x0F ? 0 : H_FLAG));
		write(read_HL(), tempValue);
		t = 12;
		break;
	case 0x35:
		// DEC (HL)
		tempValue = read(read_HL()) - 1;
		set_f(N_FLAG | (get_f() & C_FLAG) | ZeroTable[tempValue] |
			((tempValue & 0x0F) == 0x0F ? H_FLAG : 0));
		write(read_HL(), tempValue);
		t = 12;
		break;
	case 0x36:
		// LD (HL),NN
		write(read_HL(), read(reg_pc++));
		t = 12;
		break;
	case 0x37:
		// SCF
		set_f(get_f() & Z_FLAG | C_FLAG);
		t = 4;
		t = 4;
		break;
	case 0x38:
		// JR C,NN
		if (get_f()&C_FLAG) {
			reg_pc += (int8_t)read(reg_pc) + 1;
			t = 12;
		}
		else
		{
			reg_pc++;
			t = 8;
		}
		break;
	case 0x39:
		// ADD HL,SP
		tempRegister.W = (read_HL() + reg_sp) & 0xFFFF;
		set_f((get_f() & Z_FLAG) | ((read_HL()^reg_sp^tempRegister.W) & 0x1000 ? H_FLAG : 0) |
			(((long)read_HL() + (long)reg_sp) & 0x10000 ? C_FLAG : 0));
		write_HL(tempRegister.W);
		t = 8;
		break;
	case 0x3a:
		// LDD A,(HL)
		reg_a = read(read_HL());
		write_HL(read_HL() - 1);
		t = 8;
		break;
	case 0x3b:
		// DEC SP
		reg_sp--;
		t = 8;
		break;
	case 0x3c:
		// INC A
		reg_a++;
		set_f((get_f() & C_FLAG) | ZeroTable[reg_a] | (reg_a & 0x0F ? 0 : H_FLAG));
		t = 4;
		break;
	case 0x3d:
		// DEC A
		reg_a--;
		set_f(N_FLAG | (get_f() & C_FLAG) | ZeroTable[reg_a] |
			((reg_a & 0x0F) == 0x0F ? H_FLAG : 0));
		t = 4;
		break;
	case 0x3e:
		// LD A,NN
		reg_a = read(reg_pc++);
		t = 8;
		break;
	case 0x3f:
		// CCF
		set_f(get_f() ^ C_FLAG);
		set_f(get_f() & ~(N_FLAG | H_FLAG));
		t = 4;
		break;
	case 0x40:
		// LD B,B
		reg_b = reg_b;
		t = 4;
		break;
	case 0x41:
		// LD B,C
		reg_b = reg_c;
		t = 4;
		break;
	case 0x42:
		// LD B,D
		reg_b = reg_d;
		t = 4;
		break;
	case 0x43:
		// LD B,E
		reg_b = reg_e;
		t = 4;
		break;
	case 0x44:
		// LD B,H
		reg_b = reg_h;
		t = 4;
		break;
	case 0x45:
		// LD B,L
		reg_b = reg_l;
		t = 4;
		break;
	case 0x46:
		// LD B,(HL)
		reg_b = read(read_HL());
		t = 8;
		break;
	case 0x47:
		// LD B,A
		reg_b = reg_a;
		t = 4;
		break;
	case 0x48:
		// LD C,B
		reg_c = reg_b;
		t = 4;
		break;
	case 0x49:
		// LD C,C
		reg_c = reg_c;
		t = 4;
		break;
	case 0x4a:
		// LD C,D
		reg_c = reg_d;
		t = 4;
		break;
	case 0x4b:
		// LD C,E
		reg_c = reg_e;
		t = 4;
		break;
	case 0x4c:
		// LD C,H
		reg_c = reg_h;
		t = 4;
		break;
	case 0x4d:
		// LD C,L
		reg_c = reg_l;
		t = 4;
		break;
	case 0x4e:
		// LD C,(HL)
		reg_c = read(read_HL());
		t = 8;
		break;
	case 0x4f:
		// LD C,A
		reg_c = reg_a;
		t = 4;
		break;
	case 0x50:
		// LD D,B
		reg_d = reg_b;
		t = 4;
		break;
	case 0x51:
		// LD D,C
		reg_d = reg_c;
		t = 4;
		break;
	case 0x52:
		// LD D,D
		reg_d = reg_d;
		t = 4;
		break;
	case 0x53:
		// LD D,E
		reg_d = reg_e;
		t = 4;
		break;
	case 0x54:
		// LD D,H
		reg_d = reg_h;
		t = 4;
		break;
	case 0x55:
		// LD D,L
		reg_d = reg_l;
		t = 4;
		break;
	case 0x56:
		// LD D,(HL)
		reg_d = read(read_HL());
		t = 8;
		break;
	case 0x57:
		// LD D,A
		reg_d = reg_a;
		t = 4;
		break;
	case 0x58:
		// LD E,B
		reg_e = reg_b;
		t = 4;
		break;
	case 0x59:
		// LD E,C
		reg_e = reg_c;
		t = 4;
		break;
	case 0x5a:
		// LD E,D
		reg_e = reg_d;
		t = 4;
		break;
	case 0x5b:
		// LD E,E
		reg_e = reg_e;
		t = 4;
		break;
	case 0x5c:
		// LD E,H
		reg_e = reg_h;
		t = 4;
		break;
	case 0x5d:
		// LD E,L
		reg_e = reg_l;
		t = 4;
		break;
	case 0x5e:
		// LD E,(HL)
		reg_e = read(read_HL());
		t = 8;
		break;
	case 0x5f:
		// LD E,A
		reg_e = reg_a;
		t = 4;
		break;
	case 0x60:
		// LD H,B
		reg_h = reg_b;
		t = 4;
		break;
	case 0x61:
		// LD H,C
		reg_h = reg_c;
		t = 4;
		break;
	case 0x62:
		// LD H,D
		reg_h = reg_d;
		t = 4;
		break;
	case 0x63:
		// LD H,E
		reg_h = reg_e;
		t = 4;
		break;
	case 0x64:
		// LD H,H
		reg_h = reg_h;
		t = 4;
		break;
	case 0x65:
		// LD H,L
		reg_h = reg_l;
		t = 4;
		break;
	case 0x66:
		// LD H,(HL)
		reg_h = read(read_HL());
		t = 8;
		break;
	case 0x67:
		// LD H,A
		reg_h = reg_a;
		t = 4;
		break;
	case 0x68:
		// LD L,B
		reg_l = reg_b;
		t = 4;
		break;
	case 0x69:
		// LD L,C
		reg_l = reg_c;
		t = 4;
		break;
	case 0x6a:
		// LD L,D
		reg_l = reg_d;
		t = 4;
		break;
	case 0x6b:
		// LD L,E
		reg_l = reg_e;
		t = 4;
		break;
	case 0x6c:
		// LD L,H
		reg_l = reg_h;
		t = 4;
		break;
	case 0x6d:
		// LD L,L
		reg_l = reg_l;
		t = 4;
		break;
	case 0x6e:
		// LD L,(HL)
		reg_l = read(read_HL());
		t = 8;
		break;
	case 0x6f:
		// LD L,A
		reg_l = reg_a;
		t = 4;
		break;
	case 0x70:
		// LD (HL),B
		write(read_HL(), reg_b);
		t = 8;
		break;
	case 0x71:
		// LD (HL),C
		write(read_HL(), reg_c);
		t = 8;
		break;
	case 0x72:
		// LD (HL),D
		write(read_HL(), reg_d);
		t = 8;
		break;
	case 0x73:
		// LD (HL),E
		write(read_HL(), reg_e);
		t = 8;
		break;
	case 0x74:
		// LD (HL),H
		write(read_HL(), reg_h);
		t = 8;
		break;
	case 0x75:
		// LD (HL),L
		write(read_HL(), reg_l);
		t = 8;
		break;
	case 0x76:
		// HALT
		// If an EI is pending, the interrupts are triggered before Halt state !!
		// Fix Torpedo Range's intro.
		/*if (IFF & 0x40)
		{
			IFF &= ~0x70;
			IFF |= 1;
			reg_pc--;
		}
		else
		{
			// if (IE & IF) and interrupts are disabeld, 
			// Halt is cancelled.
			if ((register_IE & register_IF & 0x1f) && !(IFF & 1))
			{
				IFF |= 2;
			}
			else
				IFF |= 0x80;
		}*/
		t = 4;
		break;
	case 0x77:
		// LD (HL),A
		write(read_HL(), reg_a);
		t = 8;
		break;
	case 0x78:
		// LD A,B
		reg_a = reg_b;
		t = 4;
		break;
	case 0x79:
		// LD A,C
		reg_a = reg_c;
		t = 4;
		break;
	case 0x7a:
		// LD A,D
		reg_a = reg_d;
		t = 4;
		break;
	case 0x7b:
		// LD A,E
		reg_a = reg_e;
		t = 4;
		break;
	case 0x7c:
		// LD A,H
		reg_a = reg_h;
		t = 4;
		break;
	case 0x7d:
		// LD A,L
		reg_a = reg_l;
		t = 4;
		break;
	case 0x7e:
		// LD A,(HL)
		reg_a = read(read_HL());
		t = 8;
		break;
	case 0x7f:
		// LD A,A
		reg_a = reg_a;
		t = 4;
		break;
	case 0x80:
		// ADD B
		tempRegister.W = reg_a + reg_b;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_b^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x81:
		// ADD C
		tempRegister.W = reg_a + reg_c;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_c^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x82:
		// ADD D
		tempRegister.W = reg_a + reg_d;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_d^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x83:
		// ADD E
		tempRegister.W = reg_a + reg_e;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_e^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x84:
		// ADD H
		tempRegister.W = reg_a + reg_h;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_h^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x85:
		// ADD L
		tempRegister.W = reg_a + reg_l;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_l^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x86:
		// ADD (HL)
		tempValue = read(read_HL());
		tempRegister.W = reg_a + tempValue;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0x87:
		// ADD A
		tempRegister.W = reg_a + reg_a;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_a^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x88:
		// ADC B:
		tempRegister.W = reg_a + reg_b + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_b^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x89:
		// ADC C
		tempRegister.W = reg_a + reg_c + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_c^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x8a:
		// ADC D
		tempRegister.W = reg_a + reg_d + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_d^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x8b:
		// ADC E
		tempRegister.W = reg_a + reg_e + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_e^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x8c:
		// ADC H
		tempRegister.W = reg_a + reg_h + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_h^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x8d:
		// ADC L
		tempRegister.W = reg_a + reg_l + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_l^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x8e:
		// ADC (HL)
		tempValue = read(read_HL());
		tempRegister.W = reg_a + tempValue + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0x8f:
		// ADC A
		tempRegister.W = reg_a + reg_a + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_a^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x90:
		// SUB B
		tempRegister.W = reg_a - reg_b;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_b^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x91:
		// SUB C
		tempRegister.W = reg_a - reg_c;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_c^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x92:
		// SUB D
		tempRegister.W = reg_a - reg_d;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_d^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x93:
		// SUB E
		tempRegister.W = reg_a - reg_e;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_e^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x94:
		// SUB H
		tempRegister.W = reg_a - reg_h;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_h^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x95:
		// SUB L
		tempRegister.W = reg_a - reg_l;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_l^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x96:
		// SUB (HL)
		tempValue = read(read_HL());
		tempRegister.W = reg_a - tempValue;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0x97:
		// SUB A
		reg_a = 0;
		set_f(N_FLAG | Z_FLAG);
		t = 4;
		break;
	case 0x98:
		// SBC B
		tempRegister.W = reg_a - reg_b - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_b^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x99:
		// SBC C
		tempRegister.W = reg_a - reg_c - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_c^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x9a:
		// SBC D
		tempRegister.W = reg_a - reg_d - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_d^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x9b:
		// SBC E
		tempRegister.W = reg_a - reg_e - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_e^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x9c:
		// SBC H
		tempRegister.W = reg_a - reg_h - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_h^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x9d:
		// SBC L
		tempRegister.W = reg_a - reg_l - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_l^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0x9e:
		// SBC (HL)
		tempValue = read(read_HL());
		tempRegister.W = reg_a - tempValue - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0x9f:
		// SBC A
		tempRegister.W = reg_a - reg_a - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_a^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 4;
		break;
	case 0xa0:
		// AND B
		reg_a &= reg_b;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa1:
		// AND C
		reg_a &= reg_c;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa2:
		// AND_D
		reg_a &= reg_d;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa3:
		// AND E
		reg_a &= reg_e;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa4:
		// AND H
		reg_a &= reg_h;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa5:
		// AND L
		reg_a &= reg_l;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa6:
		// AND (HL)
		tempValue = read(read_HL());
		reg_a &= tempValue;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 8;
		break;
	case 0xa7:
		// AND A
		reg_a &= reg_a;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa8:
		// XOR B
		reg_a ^= reg_b;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xa9:
		// XOR C
		reg_a ^= reg_c;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xaa:
		// XOR D
		reg_a ^= reg_d;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xab:
		// XOR E
		reg_a ^= reg_e;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xac:
		// XOR H
		reg_a ^= reg_h;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xad:
		// XOR L
		reg_a ^= reg_l;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xae:
		// XOR (HL)
		tempValue = read(read_HL());
		reg_a ^= tempValue;
		set_f(ZeroTable[reg_a]);
		t = 8;
		break;
	case 0xaf:
		// XOR A
		reg_a = 0;
		set_f(Z_FLAG);
		t = 4;
		break;
	case 0xb0:
		// OR B
		reg_a |= reg_b;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xb1:
		// OR C
		reg_a |= reg_c;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xb2:
		// OR D
		reg_a |= reg_d;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xb3:
		// OR E
		reg_a |= reg_e;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xb4:
		// OR H
		reg_a |= reg_h;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xb5:
		// OR L
		reg_a |= reg_l;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xb6:
		// OR (HL)
		tempValue = read(read_HL());
		reg_a |= tempValue;
		set_f(ZeroTable[reg_a]);
		t = 8;
		break;
	case 0xb7:
		// OR A
		reg_a |= reg_a;
		set_f(ZeroTable[reg_a]);
		t = 4;
		break;
	case 0xb8:
		// CP B:
		tempRegister.W = reg_a - reg_b;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_b^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 4;
		break;
	case 0xb9:
		// CP C
		tempRegister.W = reg_a - reg_c;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_c^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 4;
		break;
	case 0xba:
		// CP D
		tempRegister.W = reg_a - reg_d;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_d^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 4;
		break;
	case 0xbb:
		// CP E
		tempRegister.W = reg_a - reg_e;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_e^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 4;
		break;
	case 0xbc:
		// CP H
		tempRegister.W = reg_a - reg_h;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_h^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 4;
		break;
	case 0xbd:
		// CP L
		tempRegister.W = reg_a - reg_l;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^reg_l^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 4;
		break;
	case 0xbe:
		// CP (HL)
		tempValue = read(read_HL());
		tempRegister.W = reg_a - tempValue;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 8;
		break;
	case 0xbf:
		// CP A
		set_f(N_FLAG | Z_FLAG);
		t = 4;
		break;
	case 0xc0:
		// RET NZ
		if (!(get_f()&Z_FLAG)) {
			reg_pc = (read(reg_sp) + (read(reg_sp + 1) << 8));
			reg_sp += 2;
			t = 20;
		}
		else
			t = 8;
		break;
	case 0xc1:
		// POP BC
		reg_c = read(reg_sp++);
		reg_b = read(reg_sp++);
		t = 12;
		break;
	case 0xc2:
		// JP NZ,NNNN
		if (get_f()&Z_FLAG)
		{
			reg_pc += 2;
			t = 12;
		}
		else {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc);
			reg_pc = tempRegister.W;
			t = 16;
		}
		break;
	case 0xc3:
		// JP NNNN
		tempRegister.B.B0 = read(reg_pc++);
		tempRegister.B.B1 = read(reg_pc);
		reg_pc = tempRegister.W;
		t = 16;
		break;
	case 0xc4:
		// CALL NZ,NNNN
		if (get_f()&Z_FLAG)
		{
			reg_pc += 2;
			t = 12;
		}
		else {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc++);
			write(--reg_sp, (uint8_t)(reg_pc >> 8));
			write(--reg_sp, (uint8_t)reg_pc);
			reg_pc = tempRegister.W;
			t = 24;
		}
		break;
	case 0xc5:
		// PUSH BC
		write(--reg_sp, reg_b);
		write(--reg_sp, reg_c);
		t = 16;
		break;
	case 0xc6:
		// ADD NN
		tempValue = read(reg_pc++);
		tempRegister.W = reg_a + tempValue;
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0xc7:
		// RST 00
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0000;
		t = 16;
		break;
	case 0xc8:
		// RET Z
		if (get_f()&Z_FLAG) {
			reg_pc = read(reg_sp++);
			reg_pc += read(reg_sp++) << 8;
			t = 20;
		}
		else
			t = 8;
		break;
	case 0xc9:
		// RET
		reg_pc = read(reg_sp++);
		reg_pc += read(reg_sp++) << 8;
		t = 16;
		break;
	case 0xca:
		// JP Z,NNNN
		if (get_f()&Z_FLAG) {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc);
			reg_pc = tempRegister.W;
			t = 16;
		}
		else
		{
			reg_pc += 2;
			t = 12;
		}
		break;
	case 0xcb:
		execute_CB_opcode();
		t = 4;
		break;
		// CB done outside
	case 0xcc:
		// CALL Z,NNNN
		if (get_f()&Z_FLAG) {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc++);
			write(--reg_sp, (uint8_t)(reg_pc >> 8));
			write(--reg_sp, (uint8_t)reg_pc);
			reg_pc = tempRegister.W;
			t = 24;
		}
		else
		{
			reg_pc += 2;
			t = 12;
		}
		break;
	case 0xcd:
		// CALL NNNN
		tempRegister.B.B0 = read(reg_pc++);
		tempRegister.B.B1 = read(reg_pc++);
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = tempRegister.W;
		t = 24;
		break;
	case 0xce:
		// ADC NN
		tempValue = read(reg_pc++);
		tempRegister.W = reg_a + tempValue + (get_f()&C_FLAG ? 1 : 0);
		set_f((tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0xcf:
		// RST 08
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0008;
		t = 16;
		break;
	case 0xd0:
		// RET NC
		if (!(get_f()&C_FLAG)) {
			reg_pc = read(reg_sp++);
			reg_pc += read(reg_sp++) << 8;
			t = 20;
		}
		else
			t = 8;
		break;
	case 0xd1:
		// POP DE
		reg_e = read(reg_sp++);
		reg_d = read(reg_sp++);
		t = 12;
		break;
	case 0xd2:
		// JP NC,NNNN
		if (get_f()&C_FLAG)
		{
			reg_pc += 2;
			t = 12;
		}
		else {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc);
			reg_pc = tempRegister.W;
			t = 16;
		}
		break;
		// D3 illegal
	case 0xd3:
		reg_pc--;
		//IFF = 0;
		break;
	case 0xd4:
		// CALL NC,NNNN
		if (get_f()&C_FLAG)
		{
			reg_pc += 2;
			t = 12;
		}
		else {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc++);
			write(--reg_sp, (uint8_t)(reg_pc >> 8));
			write(--reg_sp, (uint8_t)reg_pc);
			reg_pc = tempRegister.W;
			t = 24;
		}
		break;
	case 0xd5:
		// PUSH DE
		write(--reg_sp, reg_d);
		write(--reg_sp, reg_e);
		t = 16;
		break;
	case 0xd6:
		// SUB NN
		tempValue = read(reg_pc++);
		tempRegister.W = reg_a - tempValue;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0xd7:
		// RST 10
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0010;
		t = 16;
		break;
	case 0xd8:
		// RET C
		if (get_f()&C_FLAG) {
			reg_pc = read(reg_sp++);
			reg_pc += read(reg_sp++) << 8;
			t = 20;
		}
		else
			t = 8;
		break;
	case 0xd9:
		// RETI
		reg_pc = read(reg_sp++);
		reg_pc += read(reg_sp++) << 8;
		interrupts_enabled = 1;
		//IFF |= 0x01;
		t = 16;
		break;
	case 0xda:
		// JP C,NNNN
		if (get_f()&C_FLAG) {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc);
			reg_pc = tempRegister.W;
			t = 16;
		}
		else
		{
			reg_pc += 2;
			t = 12;
		}
		break;
		// DB illegal
	case 0xdb:
		reg_pc--;
		//IFF = 0;
		break;
	case 0xdc:
		// CALL C,NNNN
		if (get_f()&C_FLAG) {
			tempRegister.B.B0 = read(reg_pc++);
			tempRegister.B.B1 = read(reg_pc++);
			write(--reg_sp, (uint8_t)(reg_pc >> 8));
			write(--reg_sp, (uint8_t)reg_pc);
			reg_pc = tempRegister.W;
			t = 24;
		}
		else
		{
			reg_pc += 2;
			t = 12;
		}
		break;
		// DD illegal
	case 0xdd:
		reg_pc--;
		//IFF = 0;
		break;
	case 0xde:
		// SBC NN
		tempValue = read(reg_pc++);
		tempRegister.W = reg_a - tempValue - (get_f()&C_FLAG ? 1 : 0);
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		reg_a = tempRegister.B.B0;
		t = 8;
		break;
	case 0xdf:
		// RST 18
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0018;
		t = 16;
		break;
	case 0xe0:
		// LD (FF00+NN),A
		write(0xff00 + read(reg_pc++), reg_a);
		t = 12;
		break;
	case 0xe1:
		// POP HL
		reg_l = read(reg_sp++);
		reg_h = read(reg_sp++);
		t = 12;
		break;
	case 0xe2:
		// LD (FF00+C),A
		write(0xff00 + (uint16_t)reg_c, reg_a);
		t = 8;
		break;
		// E3 illegal
		// E4 illegal
	case 0xe3:
	case 0xe4:
		reg_pc--;
		//IFF = 0;
		break;
	case 0xe5:
		// PUSH HL
		write(--reg_sp, reg_h);
		write(--reg_sp, reg_l);
		t = 16;
		break;
	case 0xe6:
		// AND NN
		tempValue = read(reg_pc++);
		reg_a &= tempValue;
		set_f(H_FLAG | ZeroTable[reg_a]);
		t = 8;
		break;
	case 0xe7:
		// RST 20
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0020;
		t = 16;
		break;
	case 0xe8:
		// ADD SP,NN
		offset = (int8_t)read(reg_pc++);
		tempRegister.W = reg_sp + (int16_t)offset;
		set_f(0);
		if (((reg_sp ^ offset ^ (tempRegister.W & 0xFFFF)) & 0x100) == 0x100)
		{
			flag_carry = 1;
		}
		if (((reg_sp ^ offset ^ (tempRegister.W & 0xFFFF)) & 0x10) == 0x10)
		{
			flag_half_carry = 1;
		}
		reg_sp = tempRegister.W;
		t = 16;
		break;
	case 0xe9:
		// LD PC,HL
		reg_pc = read_HL();
		t = 4;
		break;
	case 0xea:
		// LD (NNNN),A
		tempRegister.B.B0 = read(reg_pc++);
		tempRegister.B.B1 = read(reg_pc++);
		write(tempRegister.W, reg_a);
		t = 16;
		break;
		// EB illegal
		// EC illegal
		// ED illegal
	case 0xeb:
	case 0xec:
	case 0xed:
		reg_pc--;
		//IFF = 0;
		break;
	case 0xee:
		// XOR NN
		tempValue = read(reg_pc++);
		reg_a ^= tempValue;
		set_f(ZeroTable[reg_a]);
		t = 8;
		break;
	case 0xef:
		// RST 28
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0028;
		t = 16;
		break;
	case 0xf0:
		// LD A,(FF00+NN)
		reg_a = read(0xff00 + read(reg_pc++));
		t = 12;
		break;
	case 0xf1:
		// POP AF
		set_f(read(reg_sp++));
		reg_a = read(reg_sp++);
		set_f(get_f() & 0xf0);
		t = 12;
		break;
	case 0xf2:
		// LD A,(FF00+C)
		reg_a = read(0xff00 + reg_c);
		t = 8;
		break;
	case 0xf3:
		// DI
		pending_disable = 1;
		t = 4;
		break;
		// F4 illegal
	case 0xf4:
		reg_pc--;
		//IFF = 0;
		break;
	case 0xf5:
		// PUSH AF
		write(--reg_sp, reg_a);
		write(--reg_sp, get_f());
		t = 16;
		break;
	case 0xf6:
		// OR NN
		tempValue = read(reg_pc++);
		reg_a |= tempValue;
		set_f(ZeroTable[reg_a]);
		t = 8;
		break;
	case 0xf7:
		// RST 30
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0030;
		t = 16;
		break;
	case 0xf8:
		// LD HL,SP+NN
		offset = (int8_t)read(reg_pc++);
		tempRegister.W = reg_sp + offset;
		set_f(0);
		if (((reg_sp ^ offset ^ tempRegister.W) & 0x100) == 0x100)
			flag_carry = 1;
		if (((reg_sp ^ offset ^ tempRegister.W) & 0x10) == 0x10)
			flag_half_carry = 1;
		write_HL(tempRegister.W);
		t = 12;
		break;
	case 0xf9:
		// LD SP,HL
		reg_sp = read_HL();
		t = 8;
		break;
	case 0xfa:
		// LD A,(NNNN)
		tempRegister.B.B0 = read(reg_pc++);
		tempRegister.B.B1 = read(reg_pc++);
		reg_a = read(tempRegister.W);
		t = 16;
		break;
	case 0xfb:
		// EI
		pending_enable = 1;
		t = 4;
		break;
		// FC illegal (FC = breakpoint)
	case 0xfc:
		//breakpoint = true;
		break;
		// FD illegal
	case 0xfd:
		reg_pc--;
		//IFF = 0;
		break;
	case 0xfe:
		// CP NN
		tempValue = read(reg_pc++);
		tempRegister.W = reg_a - tempValue;
		set_f(N_FLAG | (tempRegister.B.B1 ? C_FLAG : 0) | ZeroTable[tempRegister.B.B0] |
			((reg_a^tempValue^tempRegister.B.B0) & 0x10 ? H_FLAG : 0));
		t = 8;
		break;
	case 0xff:
		// RST 38
		write(--reg_sp, (uint8_t)(reg_pc >> 8));
		write(--reg_sp, (uint8_t)reg_pc);
		reg_pc = 0x0038;
		t = 16;
		break;
	default:
		printf("Unknown opcode %02x at %04x", opcode, read(reg_pc - 1));
		/*if (gbSystemMessage == false)
		{
			systemMessage(0, N_("Unknown opcode %02x at %04x"),
				read(reg_pc - 1), reg_pc - 1);
			gbSystemMessage = true;
		}*/
		return;

	}
}