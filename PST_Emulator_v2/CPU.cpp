#include "CPU.hpp"
#include <QThread>
#include <QCoreApplication>
#include <QMessageBox>

CPU::CPU(QObject* parent) :
	QObject(parent)
{
	reset();
}

CPU::CPU(const char* filename, QObject* parent) :
	QObject(parent)
{
	reset();
	load_rom(filename);
	m_rom_loaded = true;
}

void CPU::reset()
{
	reg_a = reg_b = reg_c = reg_d = reg_e = reg_f = reg_h = reg_l = 0;
	flag_zero = flag_carry = flag_half_carry = flag_negative = 0;
	reg_pc = 0x0000;
	reg_sp = 0xfffe;
	t = 0;
	interrupts_enabled = false;
	pending_enable = false;
	pending_disable = false;
	booting = true;
	timer_counter = 1024;
	divide_counter = 0;
	joypad_state = 0xff;
	previous_opcode = 0x00;
	tempValue = 0;
	tempRegister.W = 0;
	offset = 0;
	a = 0;
	memset(memory, 0, sizeof(memory));
	memory[0xFF00] = 0xff;
}

void CPU::load_rom(const char* filename)
{
	FILE* rom;
	fopen_s(&rom, filename, "rb");
	fread(memory, sizeof(uint8_t), 32768, rom);
	fclose(rom);
	m_rom_loaded = true;
}

CPU::~CPU()
{

}

void CPU::status()
{
	printf("a = %02x\n", reg_a);
	printf("f = %02x\n", get_f());
	printf("b = %02x\n", reg_b);
	printf("c = %02x\n", reg_c);
	printf("d = %02x\n", reg_d);
	printf("e = %02x\n", reg_e);
	printf("h = %02x\n", reg_h);
	printf("l = %02x\n", reg_l);
	printf("sp = %04x\n", reg_sp);
	printf("pc = %04x\n", reg_pc);
	printf("memory[FF00] = %02x\n", memory[0xff00]);
	printf("Joypad_State = %02x\n", joypad_state);
	printf("Joypad_State = %02x\n", get_joypad_state());
}

void CPU::step()
{
	execute_opcode();
	if (pending_enable == true && read(reg_pc - 1) != 0xfb)
	{
		interrupts_enabled = true;
		pending_enable = false;
	}
	if (pending_disable == true && read(reg_pc - 1) != 0xf3)
	{
		interrupts_enabled = false;
		pending_disable = false;
	}
	update_timers();
	do_interrupts();
}

/*TODO: make the bank switching work (MBC)*/
uint8_t CPU::read(uint16_t addr)
{
	if (booting)
	{
		if (addr < 0x0100)
			return bootstrap[addr];
		else if (reg_pc == 0x0100)
			booting = false;
	}
	if (addr == 0xff00)
	{
		return get_joypad_state();
	}
	else
		return memory[addr];
}

void CPU::write(uint16_t addr, uint8_t value)
{
	//TODO: emulation of the =/= MBC
	if (addr < 0x8000)
	{

		return;
	}
	else if (addr >= 0xE000 && addr <= 0xFDFF)
	{
		memory[addr] = value;
		write(addr - 0x2000, value);
	}
	else if (addr >= 0xFEA0 && addr < 0xFEFF)
		return;
	else if (addr == 0xFF04)
		memory[addr] = 0;
	else if (addr == 0xFF46)
		dma(value);
	else if (addr == 0xFF00)
	{
		memory[addr] = (memory[0xff00] & 0xCF) | (value & 0x30);
	}
	else
	{
		memory[addr] = value;
	}
}

void CPU::dma(uint8_t data)
{
	uint16_t addr = data << 8;
	for (int i = 0; i < 0xa0; i++)
		write(0xfe00 + i, read(addr + i));
}

uint8_t CPU::get_f()
{
	uint8_t tmp = 0;

	tmp |= (flag_zero << 7);
	tmp |= (flag_negative << 6);
	tmp |= (flag_half_carry << 5);
	tmp |= (flag_carry << 4);

	tmp |= (reg_f & 0x0f);

	return tmp;
}

uint8_t CPU::get_t()
{
	return t;
}

void CPU::set_f(uint8_t flags)
{
	reg_f = flags;
	flag_zero = (flags >> 7) & 1;
	flag_negative = (flags >> 6) & 1;
	flag_half_carry = (flags >> 5) & 1;
	flag_carry = (flags >> 4) & 1;
}

uint16_t CPU::read_AF(void)
{
	return ((reg_a << 8) + get_f());
}

uint16_t CPU::read_BC(void)
{
	return ((reg_b << 8) + reg_c);
}

uint16_t CPU::read_DE(void)
{
	return ((reg_d << 8) + reg_e);
}

uint16_t CPU::read_HL(void)
{
	return ((reg_h << 8) + reg_l);
}

void CPU::write_AF(uint16_t data)
{
	set_f((uint8_t)data);
	reg_a = (uint8_t)(data >> 8);
}

void CPU::write_BC(uint16_t data)
{
	reg_c = (uint8_t)data;
	reg_b = (uint8_t)(data >> 8);
}

void CPU::write_DE(uint16_t data)
{
	reg_e = (uint8_t)data;
	reg_d = (uint8_t)(data >> 8);
}

void CPU::write_HL(uint16_t data)
{
	reg_l = (uint8_t)data;
	reg_h = (uint8_t)(data >> 8);
}

// timer related functions
const uint16_t timer = 0xff05;
const uint16_t timer_mod = 0xff06;
const uint16_t timer_control = 0xff07;
const uint16_t divider_reg = 0xff04;

int CPU::timer_on()
{
	uint8_t control = read(timer_control);
	return (control >> 2) & 1;
}

void CPU::update_timers()
{
	divider();
	if (timer_on())
	{
		timer_counter -= t;
		if (timer_counter <= 0)
		{
			setfreq();
			uint8_t val = read(timer);
			if (timer == 0xff)
			{
				write(timer, read(timer_mod));
				// TODO: Request Interrupt
			}
			else
			{
				write(timer, val + 1);
			}
		}
	}
}

void CPU::setfreq()
{
	uint8_t	freq = read(timer_control) & 0x3;
	if (freq == 0)
		timer_counter = 1024;
	else if (freq == 1)
		timer_counter = 16;
	else if (freq == 2)
		timer_counter = 64;
	else if (freq == 3)
		timer_counter = 256;
}

void CPU::divider()
{
	divide_counter += t;
	if (divide_counter >= 255)
	{
		divide_counter = 0;
		memory[divider_reg]++;
	}
}


// interrupts
const uint16_t ier = 0xffff;
const uint16_t irr = 0xff0f;

void CPU::request_interrupt(int id)
{
	uint8_t data = read(irr);
	data |= (1 << id);
	write(irr, data);
}

void CPU::service_interrupt(int id)
{
	// disable interrupts
	interrupts_enabled = 0;

	// change the irr so that cpu knows that interrupt has been serviced
	uint8_t requests = read(irr);
	requests &= ~(1 << id);
	write(irr, requests);

	// push program counter into stack
	reg_sp--;
	write(reg_sp, (reg_pc >> 8) & 0xff);
	reg_sp--;
	write(reg_sp, reg_pc & 0xff);

	if (id == 0) // vblank
		reg_pc = 0x40;
	else if (id == 1) // lcd
		reg_pc = 0x48;
	else if (id == 2) // timer
		reg_pc = 0x50;
	else if (id == 4) // joypad
		reg_pc = 0x60;

}

void CPU::do_interrupts()
{
	if (interrupts_enabled)
	{
		uint8_t requests = read(irr);
		uint8_t enabled = read(ier);
		for (int id = 0; id <= 3; id++)
		{
			if ((requests >> id) & 1)
			{
				if ((enabled >> id) & 1)
					service_interrupt(id);
			}
		}
	}
}

uint8_t CPU::get_joypad_state()
{
	uint8_t current = memory[0xff00] & 0xF0;
	switch (current & 0x30)
	{
	case 0x10:
	{
		uint8_t topJoypad = (joypad_state >> 4) & 0x0F;
		current |= topJoypad;
		if (read(reg_pc) != 0xf0)
			joypad_state |= 0xf0;
		return current;
		break;
	}
	case 0x20:
	{
		uint8_t bottomJoypad = joypad_state & 0x0F;
		current |= bottomJoypad;
		if (read(reg_pc) != 0xf0)
			joypad_state |= 0x0f;
		return current;
		break;
	}
	case 0x30:
		return 0x0F;
		break;
	}

	return 0x0F;

}

void CPU::key_pressed(int key)
{
	joypad_state = joypad_state & (~(0x1 << key));
}

void CPU::key_released(int key)
{
	joypad_state |= 0x1 << key;
}