#include "GLRender.h"
#include "_Constants.h"
#include <gl/GLU.h>

GLRender::GLRender(int fps, QWidget* parent) : QGLWidget(parent)
{
	if (fps == 0)
		t_Timer = NULL;
	else
	{
		int seconde = 1;
		int timerInterval = seconde / fps;
		t_Timer = new QTimer(this);
		QObject::connect(t_Timer, SIGNAL(timeout()), this, SLOT(timeOutSlot()));
		t_Timer->start(timerInterval);
	}
}

void GLRender::keyPressEvent(QKeyEvent* event)
{
	switch (event->key())
	{
	case Qt::Key_Escape:
		close();
		break;
	}
}

void GLRender::timeOutSlot()
{
	if (c->m_ready)
	{
		paintGL();
		update();
	}
}

void GLRender::initializeGL()
{
	// Shader pr�-compil�
	glShadeModel(GL_SMOOTH);
	// Couleur de "clear" (fond)
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	// Effacement profondeur 1
	glClearDepth(1.0f);
	// Active la profondeur
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void GLRender::resizeGL(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(53.0f, static_cast<GLfloat>(width) / static_cast<GLfloat>(height), 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void GLRender::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1);
	float color;

	auto const x_size = 1.0f / GB_SCREEN_WIDTH;
	auto const y_size = 1.0f / GB_SCREEN_HEIGHT;

	for (int i = -GB_SCREEN_WIDTH / 2; i < GB_SCREEN_WIDTH / 2; i++)
	{
		for (int j = GB_SCREEN_HEIGHT / 2; j >  - GB_SCREEN_HEIGHT / 2; j--)
		{
			color = static_cast<float>(((*m_pixels)[i + static_cast<int>(GB_SCREEN_WIDTH) / 2][- j + static_cast<int>(GB_SCREEN_HEIGHT) / 2] == 3) ? 0 :
				0xff / ((*m_pixels)[i + static_cast<int>(GB_SCREEN_WIDTH) / 2][- j + static_cast<int>(GB_SCREEN_HEIGHT) / 2] + 1)) / 255.0f;

			glColor3f(color, color, color);


			glBegin(GL_QUADS);

			glVertex3f(i * x_size, j * y_size, 0.0f);
			glVertex3f((i + 1) * x_size, j * y_size, 0.0f);
			glVertex3f((i + 1) * x_size, (j + 1) * y_size, 0.0f);
			glVertex3f(i * x_size, (j + 1) * y_size, 0.0f);

			glEnd();
		}
	}

}

void GLRender::setPixels(int*** pixels)
{
	m_pixels = pixels;
}
