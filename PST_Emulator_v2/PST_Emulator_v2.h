#pragma once

#include "ui_PST_Emulator_v2.h"
#include "GLRender.h"
#include "Inputs.h"
#include "CPU.hpp"
#include "GPU.hpp"

class PST_Emulator_v2 : public QMainWindow
{
	Q_OBJECT

public:
	explicit PST_Emulator_v2(QWidget* parent = Q_NULLPTR);
	void setCPU(CPU* cpu);
	void setGPU(GPU* gpu);
	void startEmulation();

protected:
	void keyPressEvent(QKeyEvent* event) override;
	void keyReleaseEvent(QKeyEvent* event) override;

private:
	Ui::PST_Emulator_v2Class m_pUI;
	CPU* m_cpu;
	GPU* m_gpu;
	Inputs m_inputs;
	void setSize(float size);
	void closeEvent();

public slots:
	void slot_LoadROM();
	void slot_InputSettings();
	void slot_SetSize1x();
	void slot_SetSize2x();
	void slot_SetSize3x();
	void slot_SetSize4x();
	void slot_FullScreen();
	void emulate();
};
