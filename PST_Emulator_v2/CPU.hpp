#pragma once
#include <cstdio>
#include <cstdlib>
#include <memory>
#include <QObject>

#define C_FLAG 0x10
#define H_FLAG 0x20
#define N_FLAG 0x40
#define Z_FLAG 0x80

typedef union {
	struct {
		uint8_t B0, B1;
	} B;
	uint16_t W;
} gbRegister;

enum MemType {
	No_MBC,
	MBC1,
	MBC2,
	MBC3,
	MBC5
};

class CPU : public QObject
{
	Q_OBJECT
private:
	//general registers
	uint8_t reg_a, reg_b, reg_c, reg_d, reg_e, reg_f, reg_h, reg_l;
	//flags
	bool flag_carry, flag_half_carry, flag_negative, flag_zero;
	//program counter and stack pointer
	uint16_t reg_pc, reg_sp;

	// variable to save the number of cycles taken by last instruction
	// need to store this because it is used in gpu emulation also
	uint8_t t;

	//some flags about interrupts
	bool interrupts_enabled;
	bool pending_enable;
	bool pending_disable;

	//to know if the cpu should look in the bootstrap or in the memory
	bool booting;

	int timer_counter;
	int divide_counter;

	//to store the inputs from the users and give it to the rom when asked
	uint8_t joypad_state;

	//to save the previous opcode in order to clear the user's inputs after asked by the rom
	uint8_t previous_opcode;

	//temporary variables for opcode execution
	uint8_t tempValue;
	gbRegister tempRegister;
	int8_t offset;
	int a;

	uint8_t ZeroTable[256] = { 0x80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };


	MemType MBC_type;


	//bootstrap is the nintendo logo at the power up of the console
	uint8_t bootstrap[256] = { 0x31 , 0xFE, 0xFF, 0xAF, 0x21, 0xFF, 0x9F, 0x32, 0xCB, 0x7C, 0x20, 0xFB, 0x21, 0x26, 0xFF, 0x0E, 0x11, 0x3E, 0x80, 0x32, 0xE2, 0x0C, 0x3E, 0xF3, 0xE2, 0x32, 0x3E, 0x77, 0x77, 0x3E, 0xFC, 0xE0, 0x47, 0x21, 0x04, 0x01, 0xE5, 0x11, 0xCB, 0x00, 0x1A, 0x13, 0xBE, 0x20, 0x6B, 0x23, 0x7D, 0xFE, 0x34, 0x20, 0xF5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20, 0xFB, 0x86, 0x20, 0x5A, 0xD1, 0x21, 0x10, 0x80, 0x1A, 0xCD, 0xA9, 0x00, 0xCD, 0xAA, 0x00, 0x13, 0x7B, 0xFE, 0x34, 0x20, 0xF3, 0x3E, 0x18, 0x21, 0x2F, 0x99, 0x0E, 0x0C, 0x32, 0x3D, 0x28, 0x09, 0x0D, 0x20, 0xF9, 0x11, 0xEC, 0xFF, 0x19, 0x18, 0xF1, 0x67, 0x3E, 0x64, 0x57, 0xE0, 0x42, 0x3E, 0x91, 0xE0, 0x40, 0x04, 0x1E, 0x02, 0xCD, 0xBC, 0x00, 0x0E, 0x13, 0x24, 0x7C, 0x1E, 0x83, 0xFE, 0x62, 0x28, 0x06, 0x1E, 0xC1, 0xFE, 0x64, 0x20, 0x06, 0x7B, 0xE2, 0x0C, 0x3E, 0x87, 0xE2, 0xF0, 0x42, 0x90, 0xE0, 0x42, 0x15, 0x20, 0xDD, 0x05, 0x20, 0x69, 0x16, 0x20, 0x18, 0xD6, 0x3E, 0x91, 0xE0, 0x40, 0x1E, 0x14, 0xCD, 0xBC, 0x00, 0xF0, 0x47, 0xEE, 0xFF, 0xE0, 0x47, 0x18, 0xF3, 0x4F, 0x06, 0x04, 0xC5, 0xCB, 0x11, 0x17, 0xC1, 0xCB, 0x11, 0x17, 0x05, 0x20, 0xF5, 0x22, 0x23, 0x22, 0x23, 0xC9, 0x0E, 0x0C, 0xF0, 0x44, 0xFE, 0x90, 0x20, 0xFA, 0x0D, 0x20, 0xF7, 0x1D, 0x20, 0xF2, 0xC9, 0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99, 0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E, 0xFF, 0xFF, 0x3C, 0xE0, 0x50 };

	//memory is the total content of the memory map of the console
	//contains the ram, part of the rom, users input, ...
	//0000 - 3FFF   16KB ROM Bank 00     (in cartridge, fixed at bank 00)
	//4000 - 7FFF   16KB ROM Bank 01..NN(in cartridge, switchable bank number)
	//8000 - 9FFF   8KB Video RAM(VRAM) (switchable bank 0 - 1 in CGB Mode)
	//A000 - BFFF   8KB External RAM(in cartridge, switchable bank, if any)
	//C000 - CFFF   4KB Work RAM Bank 0 (WRAM)
	//D000 - DFFF   4KB Work RAM Bank 1 (WRAM)(switchable bank 1 - 7 in CGB Mode)
	//E000 - FDFF   Same as C000 - DDFF(ECHO)    (typically not used)
	//FE00 - FE9F   Sprite Attribute Table(OAM)
	//FEA0 - FEFF   Not Usable
	//FF00 - FF7F   I / O Ports
	//FF80 - FFFE   High RAM(HRAM)
	//FFFF        Interrupt Enable Register
	uint8_t memory[65536];



private:
	//16-bit registers operations
	uint16_t read_AF(void);
	uint16_t read_BC(void);
	uint16_t read_DE(void);
	uint16_t read_HL(void);
	void write_AF(uint16_t data);
	void write_BC(uint16_t data);
	void write_DE(uint16_t data);
	void write_HL(uint16_t data);

	//interrupts
	void do_interrupts();
	void service_interrupt(int id);

	//execution and memory access
	void dma(uint8_t data);
	void execute_opcode(void);
	void execute_CB_opcode(void);

	//clock stuff
	int timer_on();
	void update_timers();
	void setfreq();
	void divider();

	//inputs stuff
	uint8_t	get_joypad_state();

public:
	//constructors
	explicit CPU(QObject *parent = nullptr);
	explicit CPU(const char* filename, QObject* parent = nullptr);
	~CPU();

	//reset operations
	void reset();
	void load_rom(const char* filename);

	//getters
	uint8_t get_f();
	uint8_t get_t();

	//setters
	void set_f(uint8_t flags);

	//execution and memory acces
	uint8_t read(uint16_t addr);
	void write(uint16_t addr, uint8_t value);

	//interrupts
	void request_interrupt(int id);

	//inputs stuff
	void key_pressed(int key);
	void key_released(int key);

	//debug stuff
	void status(void);
	bool m_ready = false;
	bool m_rom_loaded = false;

public slots:
	void step();
};