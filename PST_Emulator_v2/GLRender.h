#pragma once

#include <QtOpenGL/QtOpenGL>
#include <QtOpenGL/QGLWidget>
#include "_Constants.h"
#include "CPU.hpp"

class GLRender : public QGLWidget
{
	Q_OBJECT
public:
	CPU* c;
	explicit GLRender(int fps = 0, QWidget * parent = 0);
	void initializeGL() override;
	void resizeGL(int width, int height) override;
	void paintGL() override;
	void keyPressEvent(QKeyEvent* event) override;
	void setPixels(int*** pixels);

public slots:
	virtual void timeOutSlot();

private:
	int ***m_pixels;
	QTimer* t_Timer;
};
