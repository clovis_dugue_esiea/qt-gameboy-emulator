#pragma once

#include "CPU.hpp"
#include "_Constants.h"

class GPU : public QObject
{
	Q_OBJECT
public:
	// values in the pixels
	int **pixels;
	// value in the clock of the gpu
	int clock;
	// the mode in which the gpu is
	// 0 - horizontal blank
	// 1 - vertical blank
	// 2 - scanline (accessing oam)
	// 3 - scanline (accessing vram)
	int mode;

	// line number being drawn
	int line;

	// pointer to the cpu with which the gpu will work
	CPU* c;

	// constructor that will set everything else to zero
	// and set cpu and the screen to the one passed
	GPU(CPU*, QObject* parent = nullptr);

	// change mode
	void change_mode(int);
	// change scanline
	void change_scanline();

	// there are two tilemaps in memory
	int which_map;

	// returns if the lcd is on or not
	// this is done by accessing the lcd control register in the
	// cpu
	bool on();

	void render_tiles();
	void render_sprites();

	int getcolor(int, uint16_t);
	int cnt;

	int count;

	// update the gpu timings and mode etc.
public slots:
	void emulate();
	void step();
signals:
	void done();
};
