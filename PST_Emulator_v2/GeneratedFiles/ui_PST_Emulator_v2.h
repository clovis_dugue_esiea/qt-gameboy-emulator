/********************************************************************************
** Form generated from reading UI file 'PST_Emulator_v2.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PST_EMULATOR_V2_H
#define UI_PST_EMULATOR_V2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PST_Emulator_v2Class
{
public:
    QAction *actionLoad_ROM;
    QAction *action2x;
    QAction *action3x;
    QAction *action4x;
    QAction *action1x;
    QAction *actionQuit;
    QAction *actionFullscreen;
    QAction *actionInputs;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuWindow;
    QMenu *menuSize;
    QMenu *menuSettings;

    void setupUi(QMainWindow *PST_Emulator_v2Class)
    {
        if (PST_Emulator_v2Class->objectName().isEmpty())
            PST_Emulator_v2Class->setObjectName(QString::fromUtf8("PST_Emulator_v2Class"));
        PST_Emulator_v2Class->resize(309, 291);
        PST_Emulator_v2Class->setMinimumSize(QSize(0, 0));
        PST_Emulator_v2Class->setMaximumSize(QSize(1000, 1000));
        actionLoad_ROM = new QAction(PST_Emulator_v2Class);
        actionLoad_ROM->setObjectName(QString::fromUtf8("actionLoad_ROM"));
        action2x = new QAction(PST_Emulator_v2Class);
        action2x->setObjectName(QString::fromUtf8("action2x"));
        action3x = new QAction(PST_Emulator_v2Class);
        action3x->setObjectName(QString::fromUtf8("action3x"));
        action4x = new QAction(PST_Emulator_v2Class);
        action4x->setObjectName(QString::fromUtf8("action4x"));
        action1x = new QAction(PST_Emulator_v2Class);
        action1x->setObjectName(QString::fromUtf8("action1x"));
        actionQuit = new QAction(PST_Emulator_v2Class);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actionFullscreen = new QAction(PST_Emulator_v2Class);
        actionFullscreen->setObjectName(QString::fromUtf8("actionFullscreen"));
        actionFullscreen->setCheckable(true);
        actionInputs = new QAction(PST_Emulator_v2Class);
        actionInputs->setObjectName(QString::fromUtf8("actionInputs"));
        centralWidget = new QWidget(PST_Emulator_v2Class);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setMinimumSize(QSize(144, 160));
        centralWidget->setMaximumSize(QSize(144, 160));
        PST_Emulator_v2Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(PST_Emulator_v2Class);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 309, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuWindow = new QMenu(menuBar);
        menuWindow->setObjectName(QString::fromUtf8("menuWindow"));
        menuSize = new QMenu(menuWindow);
        menuSize->setObjectName(QString::fromUtf8("menuSize"));
        menuSettings = new QMenu(menuBar);
        menuSettings->setObjectName(QString::fromUtf8("menuSettings"));
        PST_Emulator_v2Class->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuSettings->menuAction());
        menuBar->addAction(menuWindow->menuAction());
        menuFile->addAction(actionLoad_ROM);
        menuFile->addAction(actionQuit);
        menuWindow->addAction(menuSize->menuAction());
        menuSize->addAction(action1x);
        menuSize->addAction(action2x);
        menuSize->addAction(action3x);
        menuSize->addAction(action4x);
        menuSize->addAction(actionFullscreen);
        menuSettings->addAction(actionInputs);

        retranslateUi(PST_Emulator_v2Class);

        QMetaObject::connectSlotsByName(PST_Emulator_v2Class);
    } // setupUi

    void retranslateUi(QMainWindow *PST_Emulator_v2Class)
    {
        PST_Emulator_v2Class->setWindowTitle(QApplication::translate("PST_Emulator_v2Class", "PST_Emulator_v2", nullptr));
        actionLoad_ROM->setText(QApplication::translate("PST_Emulator_v2Class", "Load ROM", nullptr));
        action2x->setText(QApplication::translate("PST_Emulator_v2Class", "2x", nullptr));
        action3x->setText(QApplication::translate("PST_Emulator_v2Class", "3x", nullptr));
        action4x->setText(QApplication::translate("PST_Emulator_v2Class", "4x", nullptr));
        action1x->setText(QApplication::translate("PST_Emulator_v2Class", "1x", nullptr));
        actionQuit->setText(QApplication::translate("PST_Emulator_v2Class", "Quit", nullptr));
        actionFullscreen->setText(QApplication::translate("PST_Emulator_v2Class", "Fullscreen", nullptr));
#ifndef QT_NO_SHORTCUT
        actionFullscreen->setShortcut(QApplication::translate("PST_Emulator_v2Class", "F11", nullptr));
#endif // QT_NO_SHORTCUT
        actionInputs->setText(QApplication::translate("PST_Emulator_v2Class", "Inputs", nullptr));
        menuFile->setTitle(QApplication::translate("PST_Emulator_v2Class", "File", nullptr));
        menuWindow->setTitle(QApplication::translate("PST_Emulator_v2Class", "Window", nullptr));
        menuSize->setTitle(QApplication::translate("PST_Emulator_v2Class", "Size", nullptr));
        menuSettings->setTitle(QApplication::translate("PST_Emulator_v2Class", "Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PST_Emulator_v2Class: public Ui_PST_Emulator_v2Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PST_EMULATOR_V2_H
