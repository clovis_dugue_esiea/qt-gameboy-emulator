/****************************************************************************
** Meta object code from reading C++ file 'PST_Emulator_v2.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../PST_Emulator_v2.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PST_Emulator_v2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PST_Emulator_v2_t {
    QByteArrayData data[10];
    char stringdata0[133];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PST_Emulator_v2_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PST_Emulator_v2_t qt_meta_stringdata_PST_Emulator_v2 = {
    {
QT_MOC_LITERAL(0, 0, 15), // "PST_Emulator_v2"
QT_MOC_LITERAL(1, 16, 12), // "slot_LoadROM"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 18), // "slot_InputSettings"
QT_MOC_LITERAL(4, 49, 14), // "slot_SetSize1x"
QT_MOC_LITERAL(5, 64, 14), // "slot_SetSize2x"
QT_MOC_LITERAL(6, 79, 14), // "slot_SetSize3x"
QT_MOC_LITERAL(7, 94, 14), // "slot_SetSize4x"
QT_MOC_LITERAL(8, 109, 15), // "slot_FullScreen"
QT_MOC_LITERAL(9, 125, 7) // "emulate"

    },
    "PST_Emulator_v2\0slot_LoadROM\0\0"
    "slot_InputSettings\0slot_SetSize1x\0"
    "slot_SetSize2x\0slot_SetSize3x\0"
    "slot_SetSize4x\0slot_FullScreen\0emulate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PST_Emulator_v2[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    0,   55,    2, 0x0a /* Public */,
       4,    0,   56,    2, 0x0a /* Public */,
       5,    0,   57,    2, 0x0a /* Public */,
       6,    0,   58,    2, 0x0a /* Public */,
       7,    0,   59,    2, 0x0a /* Public */,
       8,    0,   60,    2, 0x0a /* Public */,
       9,    0,   61,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PST_Emulator_v2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PST_Emulator_v2 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->slot_LoadROM(); break;
        case 1: _t->slot_InputSettings(); break;
        case 2: _t->slot_SetSize1x(); break;
        case 3: _t->slot_SetSize2x(); break;
        case 4: _t->slot_SetSize3x(); break;
        case 5: _t->slot_SetSize4x(); break;
        case 6: _t->slot_FullScreen(); break;
        case 7: _t->emulate(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PST_Emulator_v2::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_PST_Emulator_v2.data,
    qt_meta_data_PST_Emulator_v2,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PST_Emulator_v2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PST_Emulator_v2::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PST_Emulator_v2.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int PST_Emulator_v2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
