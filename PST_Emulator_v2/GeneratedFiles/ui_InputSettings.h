/********************************************************************************
** Form generated from reading UI file 'InputSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INPUTSETTINGS_H
#define UI_INPUTSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_InputSettings
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label_Up;
    QLineEdit *lineEdit_Up;
    QLabel *label_Down;
    QLineEdit *lineEdit_Down;
    QLabel *label_Left;
    QLineEdit *lineEdit_Left;
    QLabel *label_Right;
    QLineEdit *lineEdit_Right;
    QLabel *label_A;
    QLineEdit *lineEdit_A;
    QLabel *label_B;
    QLineEdit *lineEdit_B;
    QLabel *label_Start;
    QLineEdit *lineEdit_Start;
    QLabel *label_Select;
    QLineEdit *lineEdit_Select;

    void setupUi(QDialog *InputSettings)
    {
        if (InputSettings->objectName().isEmpty())
            InputSettings->setObjectName(QString::fromUtf8("InputSettings"));
        InputSettings->resize(411, 191);
        InputSettings->setMinimumSize(QSize(411, 191));
        InputSettings->setMaximumSize(QSize(411, 191));
        buttonBox = new QDialogButtonBox(InputSettings);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(50, 150, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label_Up = new QLabel(InputSettings);
        label_Up->setObjectName(QString::fromUtf8("label_Up"));
        label_Up->setGeometry(QRect(20, 20, 47, 21));
        lineEdit_Up = new QLineEdit(InputSettings);
        lineEdit_Up->setObjectName(QString::fromUtf8("lineEdit_Up"));
        lineEdit_Up->setGeometry(QRect(80, 20, 113, 20));
        label_Down = new QLabel(InputSettings);
        label_Down->setObjectName(QString::fromUtf8("label_Down"));
        label_Down->setGeometry(QRect(20, 50, 47, 21));
        lineEdit_Down = new QLineEdit(InputSettings);
        lineEdit_Down->setObjectName(QString::fromUtf8("lineEdit_Down"));
        lineEdit_Down->setGeometry(QRect(80, 50, 113, 20));
        label_Left = new QLabel(InputSettings);
        label_Left->setObjectName(QString::fromUtf8("label_Left"));
        label_Left->setGeometry(QRect(20, 80, 47, 21));
        lineEdit_Left = new QLineEdit(InputSettings);
        lineEdit_Left->setObjectName(QString::fromUtf8("lineEdit_Left"));
        lineEdit_Left->setGeometry(QRect(80, 80, 113, 20));
        label_Right = new QLabel(InputSettings);
        label_Right->setObjectName(QString::fromUtf8("label_Right"));
        label_Right->setGeometry(QRect(20, 110, 47, 21));
        lineEdit_Right = new QLineEdit(InputSettings);
        lineEdit_Right->setObjectName(QString::fromUtf8("lineEdit_Right"));
        lineEdit_Right->setGeometry(QRect(80, 110, 113, 20));
        label_A = new QLabel(InputSettings);
        label_A->setObjectName(QString::fromUtf8("label_A"));
        label_A->setGeometry(QRect(220, 20, 47, 21));
        lineEdit_A = new QLineEdit(InputSettings);
        lineEdit_A->setObjectName(QString::fromUtf8("lineEdit_A"));
        lineEdit_A->setGeometry(QRect(280, 20, 113, 20));
        label_B = new QLabel(InputSettings);
        label_B->setObjectName(QString::fromUtf8("label_B"));
        label_B->setGeometry(QRect(220, 50, 47, 21));
        lineEdit_B = new QLineEdit(InputSettings);
        lineEdit_B->setObjectName(QString::fromUtf8("lineEdit_B"));
        lineEdit_B->setGeometry(QRect(280, 50, 113, 20));
        label_Start = new QLabel(InputSettings);
        label_Start->setObjectName(QString::fromUtf8("label_Start"));
        label_Start->setGeometry(QRect(220, 80, 47, 21));
        lineEdit_Start = new QLineEdit(InputSettings);
        lineEdit_Start->setObjectName(QString::fromUtf8("lineEdit_Start"));
        lineEdit_Start->setGeometry(QRect(280, 80, 113, 20));
        label_Select = new QLabel(InputSettings);
        label_Select->setObjectName(QString::fromUtf8("label_Select"));
        label_Select->setGeometry(QRect(220, 110, 47, 21));
        lineEdit_Select = new QLineEdit(InputSettings);
        lineEdit_Select->setObjectName(QString::fromUtf8("lineEdit_Select"));
        lineEdit_Select->setGeometry(QRect(280, 110, 113, 20));

        retranslateUi(InputSettings);
        QObject::connect(buttonBox, SIGNAL(accepted()), InputSettings, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), InputSettings, SLOT(reject()));

        QMetaObject::connectSlotsByName(InputSettings);
    } // setupUi

    void retranslateUi(QDialog *InputSettings)
    {
        InputSettings->setWindowTitle(QApplication::translate("InputSettings", "Inputs", nullptr));
        label_Up->setText(QApplication::translate("InputSettings", "Up", nullptr));
        label_Down->setText(QApplication::translate("InputSettings", "Down", nullptr));
        label_Left->setText(QApplication::translate("InputSettings", "Left", nullptr));
        label_Right->setText(QApplication::translate("InputSettings", "Right", nullptr));
        label_A->setText(QApplication::translate("InputSettings", "A", nullptr));
        label_B->setText(QApplication::translate("InputSettings", "B", nullptr));
        label_Start->setText(QApplication::translate("InputSettings", "Start", nullptr));
        label_Select->setText(QApplication::translate("InputSettings", "Select", nullptr));
    } // retranslateUi

};

namespace Ui {
    class InputSettings: public Ui_InputSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INPUTSETTINGS_H
