#include "PST_Emulator_v2.h"
#include "_Constants.h"

PST_Emulator_v2::PST_Emulator_v2(QWidget* parent) :
	QMainWindow(parent)
{
	// Load UI file
	m_pUI.setupUi(this);

	// Create render widget
	auto const gl = new GLRender(60, parent);
	// Set render as center widget
	setCentralWidget(gl);

	// Set minimum size of the window as the GameBoy default resolution + UI
	setMinimumSize(GB_SCREEN_WIDTH, menuBar()->height() + GB_SCREEN_HEIGHT);

	// Don't allow resize with mouse
	setWindowFlag(Qt::MSWindowsFixedSizeDialogHint);

	// Setup actions
	// File
	// -Load ROM
	QObject::connect(m_pUI.actionLoad_ROM, SIGNAL(triggered()), this, SLOT(slot_LoadROM()));
	// -Quit
	QObject::connect(m_pUI.actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
	// Settings
	// -Inputs
	QObject::connect(m_pUI.actionInputs, SIGNAL(triggered()), this, SLOT(slot_InputSettings()));
	// Window
	// -Resize
	QObject::connect(m_pUI.action1x, SIGNAL(triggered()), this, SLOT(slot_SetSize1x()));
	QObject::connect(m_pUI.action2x, SIGNAL(triggered()), this, SLOT(slot_SetSize2x()));
	QObject::connect(m_pUI.action3x, SIGNAL(triggered()), this, SLOT(slot_SetSize3x()));
	QObject::connect(m_pUI.action4x, SIGNAL(triggered()), this, SLOT(slot_SetSize4x()));
	// -Fullscreen
	addAction(m_pUI.actionFullscreen);
	QObject::connect(m_pUI.actionFullscreen, SIGNAL(triggered()), this, SLOT(slot_FullScreen()));

	// Loading user settings
	// TODO : Remove hardcoded settings, use QSettings for creation of a .ini file
	// Force x2 size
	setSize(2);
}

void PST_Emulator_v2::setCPU(CPU* cpu)
{
	m_cpu = cpu;
	dynamic_cast<GLRender*>(this->centralWidget())->c = cpu;
}

void PST_Emulator_v2::setGPU(GPU* gpu)
{
	m_gpu = gpu;
}

void PST_Emulator_v2::slot_LoadROM()
{
	QString filename = QFileDialog::getOpenFileName(this,
		tr("Open ROM"), "", tr("GameBoy ROM (*.gb)"));
	m_cpu->reset();
	m_cpu->load_rom(filename.toStdString().c_str());
}
void PST_Emulator_v2::slot_InputSettings()
{
	m_inputs.setWindowFlag(Qt::MSWindowsFixedSizeDialogHint);
	m_inputs.show();
}
void PST_Emulator_v2::setSize(float size)
{
	int x1 = 0, y1 = 0, x2, y2;
	geometry().getCoords(&x1, &y1, &x2, &y2);
	setGeometry(x1, y1, GB_SCREEN_WIDTH * size, menuBar()->height() + (GB_SCREEN_HEIGHT) * size);
}

void PST_Emulator_v2::slot_SetSize1x() { setSize(1); }
void PST_Emulator_v2::slot_SetSize2x() { setSize(2); }
void PST_Emulator_v2::slot_SetSize3x() { setSize(3); }
void PST_Emulator_v2::slot_SetSize4x() { setSize(4); }
void PST_Emulator_v2::slot_FullScreen()
{
	if (m_pUI.actionFullscreen->isChecked()) {
		showFullScreen();
		m_pUI.menuBar->setVisible(false);
	}
	else {
		m_pUI.menuBar->setVisible(true);
		showNormal();
	}
}

void PST_Emulator_v2::emulate()
{
	// TODO : Create Emulator superclass wrapping cpu and gpu
	m_gpu->emulate();
}


void PST_Emulator_v2::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == m_inputs.getKeys()[K_Up].code)
	{
		m_cpu->key_pressed(2);
	}
	else if (event->key() == m_inputs.getKeys()[K_Down].code)
	{
		m_cpu->key_pressed(3);
	}
	else if (event->key() == m_inputs.getKeys()[K_Left].code)
	{
		m_cpu->key_pressed(1);
	}
	else if (event->key() == m_inputs.getKeys()[K_Right].code)
	{
		m_cpu->key_pressed(0);
	}
	else if (event->key() == m_inputs.getKeys()[K_A].code)
	{
		m_cpu->key_pressed(4);
	}
	else if (event->key() == m_inputs.getKeys()[K_B].code)
	{
		m_cpu->key_pressed(5);
	}
	else if (event->key() == m_inputs.getKeys()[K_Start].code)
	{
		m_cpu->key_pressed(7);
	}
	else if (event->key() == m_inputs.getKeys()[K_Select].code)
	{
		m_cpu->key_pressed(6);
	}
}
void PST_Emulator_v2::keyReleaseEvent(QKeyEvent* event)
{
}

void PST_Emulator_v2::startEmulation()
{
	auto thread = new QThread(this);
	this->m_cpu->moveToThread(thread);
	this->m_gpu->moveToThread(thread);
	QObject::connect(thread, SIGNAL(started()), m_gpu, SLOT(emulate()));
	// TODO : Manage thread clean exit
	//QObject::connect(thread, SIGNAL(QThread::finished()), this, SLOT(QObject::deleteLater()));
	QObject::connect(qApp, SIGNAL(aboutToQuit()), thread, SLOT(terminate()));
	thread->start();
}

