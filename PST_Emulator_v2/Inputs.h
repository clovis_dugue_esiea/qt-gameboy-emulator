#pragma once
#include "ui_InputSettings.h"

struct s_key
{
	char name[32];
	int code;
};

class Inputs : public QDialog
{
	Q_OBJECT
public:
	Inputs();
	bool eventFilter(QObject* pObject, QEvent* pEvent) override;
	struct s_key* getKeys() { return m_keys; }
	Ui::InputSettings w;
private:
	struct s_key m_keys[8], m_pending_keys[8];
	static void keyName(QKeyEvent& pEvent, char* buffer);
public slots:
	void loadSettings();
	void saveSettings();
};

