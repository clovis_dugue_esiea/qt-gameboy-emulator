#include "PST_Emulator_v2.h"
#include <QtWidgets/QApplication>
#include "CPU.hpp"
#include "GPU.hpp"

int main(int argc, char *argv[])
{
	// Create the application
	QApplication a(argc, argv);

	// If there are too many arguments
	if (argc > 2)
	{
		printf("Unexpected number of arguments\n");
		a.quit();
		return -1;
	}

	// Spawn the main window
	PST_Emulator_v2 w;
	w.show();

	// Spawn the CPU
	CPU cpu;

	// Spawn the GPU
	GPU gpu(&cpu);

	// If there are two arguments, load the second argument as a ROM file
	if (argc == 2)
	{
		// Spawn the CPU
		cpu.load_rom(argv[1]);
	}

	// Link the GPU pixel map to the renderer
	static_cast<GLRender*>(w.centralWidget())->setPixels(&(gpu.pixels));

	// Link CPU and GPU to the main process
	w.setCPU(&cpu);
	w.setGPU(&gpu);
	w.startEmulation();

	// Launch the main loop
	return a.exec();
}
