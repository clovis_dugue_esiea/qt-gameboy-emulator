#include "Inputs.h"
#include <QKeyEvent>

Inputs::Inputs()
{
	// Setup UI created in Qt Designer
	w.setupUi(this);

	// Install all the event filters
	w.lineEdit_Up->installEventFilter(this);
	w.lineEdit_Down->installEventFilter(this);
	w.lineEdit_Left->installEventFilter(this);
	w.lineEdit_Right->installEventFilter(this);
	w.lineEdit_A->installEventFilter(this);
	w.lineEdit_B->installEventFilter(this);
	w.lineEdit_Start->installEventFilter(this);
	w.lineEdit_Select->installEventFilter(this);

	// Connect the ok and cancel buttons to custom slots
	QObject::connect(w.buttonBox, SIGNAL(accepted()), this, SLOT(saveSettings()));
	QObject::connect(w.buttonBox, SIGNAL(rejected()), this, SLOT(loadSettings()));
}

bool Inputs::eventFilter(QObject* pObject, QEvent* pEvent)
{
	if (pEvent->type() == QEvent::KeyPress)
	{
		auto pKeyEvent = static_cast<QKeyEvent*>(pEvent);
		char text_buffer[32];

		if (strcmp(pObject->metaObject()->className(), "QLineEdit") == 0)
		{
			keyName(*pKeyEvent, text_buffer);
			QLineEdit* pLineEdit = static_cast<QLineEdit*>(pObject);
			pLineEdit->setText(text_buffer);

			if (pObject == w.lineEdit_Up)
			{
				m_pending_keys[0].code = pKeyEvent->key();
				strcpy(m_pending_keys[0].name, text_buffer);
			}
			else if (pObject == w.lineEdit_Down)
			{
				m_pending_keys[1].code = pKeyEvent->key();
				strcpy(m_pending_keys[1].name, text_buffer);
			}
			else if (pObject == w.lineEdit_Left)
			{
				m_pending_keys[2].code = pKeyEvent->key();
				strcpy(m_pending_keys[2].name, text_buffer);
			}
			else if (pObject == w.lineEdit_Right)
			{
				m_pending_keys[3].code = pKeyEvent->key();
				strcpy(m_pending_keys[3].name, text_buffer);
			}
			else if (pObject == w.lineEdit_A)
			{
				m_pending_keys[4].code = pKeyEvent->key();
				strcpy(m_pending_keys[4].name, text_buffer);
			}
			else if (pObject == w.lineEdit_B)
			{
				m_pending_keys[5].code = pKeyEvent->key();
				strcpy(m_pending_keys[5].name, text_buffer);
			}
			else if (pObject == w.lineEdit_Start)
			{
				m_pending_keys[6].code = pKeyEvent->key();
				strcpy(m_pending_keys[6].name, text_buffer);
			}
			else if (pObject == w.lineEdit_Select)
			{
				m_pending_keys[7].code = pKeyEvent->key();
				strcpy(m_pending_keys[7].name, text_buffer);
			}

			return true;
		}
	}

	return QDialog::eventFilter(pObject, pEvent);
}

void Inputs::keyName(QKeyEvent& pEvent, char* buffer)
{
	switch (pEvent.key())
	{
	case Qt::Key_Control:
		strcpy(buffer, "CONTROL");
		break;
	case Qt::Key_Alt:
		strcpy(buffer, "ALT");
		break;
	case Qt::Key_Enter:
		strcpy(buffer, "ENTER");
		break;
	case Qt::Key_Shift:
		strcpy(buffer, "SHIFT");
		break;
	case Qt::Key_Backspace:
		strcpy(buffer, "BACKSPACE");
		break;
	case Qt::Key_Up:
		strcpy(buffer, "UP");
		break;
	case Qt::Key_Left:
		strcpy(buffer, "LEFT");
		break;
	case Qt::Key_Right:
		strcpy(buffer, "RIGHT");
		break;
	case Qt::Key_Down:
		strcpy(buffer, "DOWN");
		break;
	case Qt::Key_Return:
		strcpy(buffer, "RETURN");
		break;
	case Qt::Key_Space:
		strcpy(buffer, "SPACE");
		break;
	case Qt::Key_Tab:
		strcpy(buffer, "TAB");
		break;
	case Qt::Key_Home:
		strcpy(buffer, "HOME");
		break;
	case Qt::Key_End:
		strcpy(buffer, "END");
		break;
	case Qt::Key_PageUp:
		strcpy(buffer, "PAGE UP");
		break;
	case Qt::Key_PageDown:
		strcpy(buffer, "PAGE DOWN");
		break;
	case Qt::Key_Insert:
		strcpy(buffer, "INSERT");
		break;
	case Qt::Key_Delete:
		strcpy(buffer, "DELETE");
		break;
	default:
		strcpy(buffer, pEvent.text().toUpper().toLatin1());
	}
}

void Inputs::loadSettings()
{
	for (int i = 0; i < 8; i++)
	{
		m_pending_keys[i] = m_keys[i];
	}
}

void Inputs::saveSettings()
{
	for (int i = 0; i < 8; i++)
	{
		m_keys[i] = m_pending_keys[i];
	}
}