#pragma once

#define GB_SCREEN_HEIGHT 144.0f
#define GB_SCREEN_WIDTH 160.0f
#define GB_FRAMERATE 59.96

enum
{
	K_Up,
	K_Down,
	K_Left,
	K_Right,
	K_A,
	K_B,
	K_Start,
	K_Select
};